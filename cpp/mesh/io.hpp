#pragma once

#include "nlohmann/json_fwd.hpp"
#include "model.hpp"

namespace gmsh_interface::io
{

    struct ParsedNode
    {
        std::string uuid;
        double x;
        double y;
    };

    struct ParsedEdge
    {
        std::string uuid;
        std::string first_node;
        std::string second_node;
    };

    struct ParsedLoop
    {
        std::string uuid;
        std::vector<std::string> edges;
    };

    struct ParsedModel
    {
        std::vector<ParsedNode> nodes;
        std::vector<ParsedEdge> edges;
        std::vector<ParsedLoop> loops;
    };

    void from_json(const nlohmann::json& j, ParsedNode& n);
    void from_json(const nlohmann::json& j, ParsedEdge& n);
    void from_json(const nlohmann::json& j, ParsedLoop& n);
    void from_json(const nlohmann::json& j, ParsedModel& m);

    // Reads a model from the input json string "json" Returns the geometric
    // model, and the mappings between the uuid-s in the json string and the
    // resuling model
    std::tuple<GeometricModel, UuidToModelKeyMap> read(const std::string& json);

    // Helper function, I might move it to a private header later
    std::tuple<GeometricModel, UuidToModelKeyMap> from_parsed(const ParsedModel& parsed_model);
}