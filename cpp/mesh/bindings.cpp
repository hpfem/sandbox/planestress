#include "gmsh_interface.hpp"
#include "mesh.hpp"

#include <emscripten/bind.h>
#include <iostream>
//#include <sanitizer/lsan_interface.h>

namespace
{

struct MeshResult
{
    traceshared::Mesh mesh;
    gmsh_interface::UuidToNodes uuid_to_nodes;
};

MeshResult mesh_json(const std::string& json, double mesh_size_factor)
{
    try
    {
        MeshResult result;
        std::tie(result.mesh, result.uuid_to_nodes) = gmsh_interface::mesh_json(json, mesh_size_factor);
        return result;
    }
    catch(const std::exception& e)
    {
        std::cout << "Something went wrong when generating the mesh.\nGot error message: " << e.what() << std::endl;
        return {{}, {}};
    }
    
}

}

EMSCRIPTEN_BINDINGS(emtrace)
{
    emscripten::register_vector<size_t>("SizeTVector");
    // TODO (laszlo: I need to understand why do I have to register a string vector)
    emscripten::register_vector<std::string>("StringVector"); 
    emscripten::register_map<std::string, std::vector<size_t>>("StringToSizeTVectorMap");
    emscripten::class_<traceshared::Mesh>("Mesh");

    emscripten::class_<MeshResult>("MeshResult")
    .property("mesh", &MeshResult::mesh)
    .property("uuid_to_nodes", &MeshResult::uuid_to_nodes);

    emscripten::function("mesh_json", &mesh_json);
    emscripten::function("from_json", &traceshared::from_json);
    emscripten::function("mesh_to_svg", &traceshared::to_svg);
    emscripten::function("mesh_to_json", &traceshared::to_json);
    emscripten::function("uuid_map_to_json", &gmsh_interface::to_json);
}