#pragma once

#include <cstdlib>
#include <map>
#include <string>
#include <vector>

namespace gmsh_interface
{

using NodeKey = size_t;
using EdgeKey = size_t;

struct Node
{
    double x;
    double y;
};

using Nodes = std::vector<Node>;

struct Edge
{
    NodeKey firstNode;
    NodeKey lastNode;
};

using Edges = std::vector<Edge>;

struct Loop
{
    std::vector<EdgeKey> edges;
};

using Loops = std::vector<Loop>;

struct GeometricModel
{
    Nodes nodes;
    Edges edges;
    Loops loops;
};

struct UuidToModelKeyMap
{
    std::map<std::string, NodeKey> nodeKeyMap;
    std::map<std::string, EdgeKey> edgeKeyMap;
};

}