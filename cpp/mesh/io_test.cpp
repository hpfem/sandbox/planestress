#include "io.hpp"
#include "model.hpp"

#include "nlohmann/json.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

constexpr std::string_view node_json = R"""(
    {
        "id": "17071971-7f8b-4605-8e49-f9ec757d2659",
        "x": 12.0,
        "y": 17029935.0
    }
)""";

constexpr std::string_view edge_json = R"""(
    {
        "id": "fa185302-fb73-4e84-8b4d-9910f00838b5",
        "first_node": "46900f27-8f79-44c7-b212-9b587b2771bc",
        "second_node": "8212881a-5a05-4ef6-8122-5bd1087ca1dc"
    }
)""";

constexpr std::string_view loop_json = R"""(
    {
        "id": "fa185302-fb73-4e84-8b4d-9910f00838b5",
        "edges": [
            "46900f27-8f79-44c7-b212-9b587b2771bc",
            "8212881a-5a05-4ef6-8122-5bd1087ca1dc"
        ]
    }
)""";

constexpr std::string_view model_json = R"""(
    {
        "nodes":[
            {"id": "1", "x": 0.0, "y": 0.0 },
            {"id": "2", "x": 1.0, "y": 0.0 },
            {"id": "3", "x": 0.0, "y": 1.0 }
        ],
        "edges":[
            {"id": "4", "first_node": "1", "second_node": "2"},
            {"id": "5", "first_node": "2", "second_node": "3"},
            {"id": "6", "first_node": "3", "second_node": "1"}
        ],
        "loops":[
            {"id": "7", "edges": ["4", "5", "6"]}
        ] 
    }
)""";

constexpr std::string_view invalid_model_json = R"""(
    {
        "nodes":[
            {"id": "1", "x": 0.0, "y": 0.0 },
            {"id": "2", "x": 1.0, "y": 0.0 }
        ],
        "edges":[
            {"id": "4", "first_node": "1", "second_node": "2"},
            {"id": "5", "first_node": "2", "second_node": "3"}
        ],
        "loops":[
             {"id": "7", "edges": ["4", "5", "6"]}
            ]
    }
)""";



using namespace gmsh_interface::io;


TEST_CASE("Parse node")
{
    nlohmann::json j = nlohmann::json::parse(node_json);
    auto parsedNode = j.get<ParsedNode>();

    CHECK(parsedNode.uuid == "17071971-7f8b-4605-8e49-f9ec757d2659");
    CHECK_THAT(parsedNode.x, Catch::Matchers::WithinAbs(12.0, 1E-6));
    CHECK_THAT(parsedNode.y, Catch::Matchers::WithinAbs(17029935.0, 1E-6));
}

TEST_CASE("Parse edge")
{
    nlohmann::json j = nlohmann::json::parse(edge_json);
    auto parsedEdge = j.get<ParsedEdge>();

    CHECK(parsedEdge.uuid == "fa185302-fb73-4e84-8b4d-9910f00838b5");
    CHECK(parsedEdge.first_node == "46900f27-8f79-44c7-b212-9b587b2771bc");
    CHECK(parsedEdge.second_node == "8212881a-5a05-4ef6-8122-5bd1087ca1dc");
}

TEST_CASE("Parse loop")
{
    nlohmann::json j = nlohmann::json::parse(loop_json);
    auto parsedLoop = j.get<ParsedLoop>();

    CHECK(parsedLoop.uuid == "fa185302-fb73-4e84-8b4d-9910f00838b5");
    CHECK(parsedLoop.edges.size() == 2);
}

TEST_CASE("Read valid model")
{
    nlohmann::json j = nlohmann::json::parse(model_json);
    auto parsedModel = j.get<ParsedModel>();

    auto [m, uuid_to_geometry] = from_parsed(parsedModel);

    CHECK(m.nodes.size() == 3);
    CHECK(m.edges.size() == 3);
    CHECK(m.loops[0].edges.size() == 3);

    CHECK(uuid_to_geometry.nodeKeyMap.size() == 3);
    CHECK(uuid_to_geometry.edgeKeyMap.size() == 3);
}

TEST_CASE("Read invalid model")
{
    nlohmann::json j = nlohmann::json::parse(invalid_model_json);
    auto parsedModel = j.get<ParsedModel>();

    REQUIRE_THROWS(from_parsed(parsedModel));
}