#pragma once

#include "mesh.hpp"
#include "model.hpp"

#include <string>
#include <vector>

namespace gmsh_interface
{

class GeometricModel;
class UuidToModelKeyMap;

using NodeKeyToMeshNodeMap = std::map<NodeKey, size_t>;
using EdgeKeyToMeshNodeMap = std::map<EdgeKey, std::vector<size_t>>;

using UuidToNodes = std::map<std::string, std::vector<size_t>>;

std::tuple<traceshared::Mesh, UuidToNodes> mesh_json(const std::string model_json, double mesh_size_factor = 1.0);
std::tuple<traceshared::Mesh, NodeKeyToMeshNodeMap, EdgeKeyToMeshNodeMap> generate_mesh(const GeometricModel& model, double mesh_size_factor = 1.0);

std::string to_json(const UuidToNodes& uuid_to_nodes);
}