#include "gmsh_interface.hpp"

#include "io.hpp"
#include "model.hpp"
#include "gmsh.h"

#include "nlohmann/json.hpp"

#include <map>
#include <optional>
#include <sstream>
#include <iostream>

namespace gmsh_interface
{

namespace 
{
    // RAII-style wrapper around gmsh initializer and finalizer
    // Finalizes gmsh automatically at scope exit (return or when exception was thrown)
    struct ScopedGmshInitialize
    {
        ScopedGmshInitialize() { gmsh::initialize(); }
        ~ScopedGmshInitialize() { gmsh::finalize(); }
    };
}

std::tuple<traceshared::Mesh, UuidToNodes> mesh_json(const std::string model_json, double mesh_size_factor)
{
    auto [model, uuid_to_geometry] = io::read(model_json);
    const auto& [mesh, node_key_to_mesh_node, edge_key_to_mesh_nodes] = generate_mesh(model, mesh_size_factor);

    UuidToNodes uuid_to_nodes;
    for(const auto& [uuid, node_key] : uuid_to_geometry.nodeKeyMap)
    {
        uuid_to_nodes.insert({uuid, {node_key_to_mesh_node.at(node_key)}});
    }

    for(const auto& [uuid, edge_key] : uuid_to_geometry.edgeKeyMap)
    {
        uuid_to_nodes.insert({uuid, edge_key_to_mesh_nodes.at(edge_key)});
    }

    return {mesh, uuid_to_nodes};
}

std::tuple<traceshared::Mesh, NodeKeyToMeshNodeMap, EdgeKeyToMeshNodeMap> generate_mesh(const GeometricModel& model, double mesh_size_factor)
{
    // Empty input model should produce empty mesh
    if(model.nodes.size() == 0)
    {
        return {{}, {}, {}};
    }

    ScopedGmshInitialize scoped_gmsh;

    std::map<size_t, int> node_id_to_gmsh_tag;
    for(size_t i = 0; i < model.nodes.size(); ++i)
    {
        const auto& node = model.nodes[i];
        auto tag = gmsh::model::geo::addPoint(node.x, node.y, 0.0, 0.0, -1);
        node_id_to_gmsh_tag.insert({i, tag});
        gmsh::model::geo::addPhysicalGroup(0, {static_cast<int>(tag)}, i, "");
    }

    std::map<size_t, int> edge_id_to_gmsh_tag;
    for(size_t i = 0; i < model.edges.size(); ++i)
    {
        const auto& edge = model.edges[i];
        auto startTag = node_id_to_gmsh_tag[edge.firstNode];
        auto endTag = node_id_to_gmsh_tag[edge.lastNode];
        auto tag = gmsh::model::geo::addLine(startTag, endTag);
        edge_id_to_gmsh_tag.insert({i, tag});
        gmsh::model::geo::addPhysicalGroup(1, {static_cast<int>(tag)}, i, "");
    }

    std::vector<int> loops;
    for(size_t i = 0; i < model.loops.size(); ++i)
    {
        std::vector<int> curveLoop;
        curveLoop.reserve(model.loops[i].edges.size());
        for (size_t j = 0; j < model.loops[i].edges.size(); ++j)
        {
            curveLoop.push_back(edge_id_to_gmsh_tag[model.loops[i].edges[j]]);
        }
        loops.push_back(gmsh::model::geo::addCurveLoop(curveLoop));
    }
    gmsh::model::geo::addPlaneSurface({loops});

    gmsh::model::geo::synchronize();

    gmsh::option::setNumber("Mesh.MeshSizeFactor", mesh_size_factor);
    gmsh::model::mesh::generate(2);

    std::vector<int> elementTypes;
    std::vector<std::vector<size_t>> elementTags;
    std::vector<std::vector<size_t>> elementNodeTags;
    gmsh::model::mesh::getElements(elementTypes, elementTags, elementNodeTags, 2, -1);


    std::vector<size_t> nodeTags;
    std::vector<double> nodeCoordinates;
    std::vector<double> parametricCoord;
    gmsh::model::mesh::getNodes(nodeTags, nodeCoordinates, parametricCoord);

    traceshared::Mesh result;

    result.coordinates.reserve(2 * nodeCoordinates.size());
    std::map<int, size_t> gmsh_tag_to_node_id;
    size_t current_node_id = 0;
    for(size_t i = 0; i < nodeTags.size(); ++i)
    {
        result.coordinates.push_back(nodeCoordinates[3 * i]);
        result.coordinates.push_back(nodeCoordinates[3 * i + 1]);
        gmsh_tag_to_node_id.insert({nodeTags[i], current_node_id});
        current_node_id++;
    }

    if(elementTypes.size() != 1)
    {
        throw std::runtime_error("A mesh was produced with more than one type of elements. This should have not happened!");
    }

    constexpr int gmshTriangleTypeId = 2;
    if(elementTypes[0] != gmshTriangleTypeId) 
    {
        throw std::runtime_error("A mesh was produced that contains elements that are not triangles. This should have not happened!");
    }

    const size_t numberOfElements = elementTags[0].size();

    result.connectivities.reserve(3 * numberOfElements);
    result.offsets.reserve(numberOfElements);
    for(size_t i = 0; i < numberOfElements; ++i)
    {
        for(size_t j = 0; j < 3; ++j)
        {
            auto nodeTag = elementNodeTags[0][3 * i + j];
            result.connectivities.push_back(gmsh_tag_to_node_id[nodeTag]);
        }
        result.offsets.push_back(result.connectivities.size() - 3);
    }

    NodeKeyToMeshNodeMap node_key_to_mesh_node;

    for(size_t i = 0; i < model.nodes.size(); ++i)
    {
        std::vector<size_t> gmsh_node_tags;
        std::vector<double> node_coords;
        gmsh::model::mesh::getNodesForPhysicalGroup(0, i, gmsh_node_tags, node_coords);
        if(gmsh_node_tags.size() != 1)
        {
            throw std::runtime_error("An input model node has more than one associated node in the mesh.");
        }
        node_key_to_mesh_node.insert({i, gmsh_tag_to_node_id[gmsh_node_tags[0]]});
    }

    EdgeKeyToMeshNodeMap edge_key_to_mesh_node;
    for(size_t i = 0; i < model.edges.size(); ++i)
    {
        std::vector<size_t> gmsh_node_tags;
        std::vector<double> node_coords;
        gmsh::model::mesh::getNodesForPhysicalGroup(1, i, gmsh_node_tags, node_coords);
        edge_key_to_mesh_node.insert({i, {}});
        std::transform(gmsh_node_tags.begin(), gmsh_node_tags.end(), std::back_inserter(edge_key_to_mesh_node[i]), [&gmsh_tag_to_node_id](const auto& gmsh_node_tag){
            return gmsh_tag_to_node_id[gmsh_node_tag];
        });
    }

    return {result, node_key_to_mesh_node, edge_key_to_mesh_node};
}

std::string to_json(const UuidToNodes& uuid_to_nodes)
{
    std::stringstream ss;
    nlohmann::json j = uuid_to_nodes;
    ss << j;
    return ss.str();
}

}