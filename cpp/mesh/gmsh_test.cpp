#include "gmsh_interface.hpp"
#include "model.hpp"

#include <catch2/catch_test_macros.hpp>

namespace
{
gmsh_interface::GeometricModel make_rectangle()
{
    gmsh_interface::GeometricModel m;
    m.nodes = {
        gmsh_interface::Node{0.0, 0.0},
        gmsh_interface::Node{1.0, 0.0},
        gmsh_interface::Node{1.0, 1.0},
        gmsh_interface::Node{0.0, 1.0},
    };

    m.edges = {
        gmsh_interface::Edge{0, 1},
        gmsh_interface::Edge{1, 2},
        gmsh_interface::Edge{2, 3},
        gmsh_interface::Edge{3, 0},
    };

    m.loops = {gmsh_interface::Loop{{0, 1, 2, 3}}};

    return m;
}
}

TEST_CASE("generate_mesh")
{
    auto m = make_rectangle();
    traceshared::Mesh mesh;
    gmsh_interface::NodeKeyToMeshNodeMap node_to_mesh_node;
    gmsh_interface::EdgeKeyToMeshNodeMap edge_to_mesh_node;

    REQUIRE_NOTHROW(std::tie(mesh, node_to_mesh_node, edge_to_mesh_node) = gmsh_interface::generate_mesh(m));

    // TODO (laszlo): test also the node mappings, not just the number of elements
    REQUIRE(mesh.coordinates.size() != 0);
    REQUIRE(mesh.connectivities.size() != 0);
    REQUIRE(mesh.offsets.size() == mesh.connectivities.size() / 3);
}

TEST_CASE("generate mesh then create svg")
{
    auto m = make_rectangle();

    traceshared::Mesh mesh;
    std::tie(mesh, std::ignore, std::ignore) = gmsh_interface::generate_mesh(m);

    std::string svg;

    REQUIRE_NOTHROW(svg = traceshared::to_svg(mesh));
    REQUIRE(!svg.empty());
}

TEST_CASE("generate mesh on empty model")
{
    auto m = gmsh_interface::GeometricModel{};

    const auto& [mesh, node_map, edge_map] = gmsh_interface::generate_mesh(m);

    REQUIRE(mesh.coordinates.size() == 0);
    REQUIRE(mesh.offsets.size() == 0);
    REQUIRE(mesh.connectivities.size() == 0);
}

// This test gets stuck when built with emscripten.
// It might be some bug in gmsh that only shows itself if built
// in the wasm context.
//TEST_CASE("generate mesh on self intersecting polygon")
//{
//    gmsh_interface::GeometricModel m{
//        .nodes{{0.0, 0.0}, {1.0, 0.0}, {0.75, -1.0}, {0.25, 1.0}},
//        .edges{{0, 1}, {1, 2}, {2, 3}, {3, 0}},
//        .loop{{0, 1, 2, 3}}
//    };
//
//
//    REQUIRE_THROWS(gmsh_interface::generate_mesh(m));
//}

