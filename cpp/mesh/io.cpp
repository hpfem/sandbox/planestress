#include "io.hpp"
#include "model.hpp"
#include "nlohmann/json.hpp"

#include "uuid/uuid.h"

#include <iostream>

namespace gmsh_interface::io
{

void from_json(const nlohmann::json& j, ParsedNode& parsed_object)
{
    j["id"].get_to(parsed_object.uuid);
    j["x"].get_to(parsed_object.x);
    j["y"].get_to(parsed_object.y);
}

void from_json(const nlohmann::json& j, ParsedEdge& parsed_object)
{
    j["id"].get_to(parsed_object.uuid);
    j["first_node"].get_to(parsed_object.first_node);
    j["second_node"].get_to(parsed_object.second_node);
}

void from_json(const nlohmann::json& j, ParsedLoop& parsed_object)
{
    j["id"].get_to(parsed_object.uuid);
    j["edges"].get_to(parsed_object.edges);
}

void from_json(const nlohmann::json& j, ParsedModel& parsed_model)
{
    j["nodes"].get_to(parsed_model.nodes);
    j["edges"].get_to(parsed_model.edges);
    j["loops"].get_to(parsed_model.loops);
}

std::tuple<GeometricModel, UuidToModelKeyMap> read(const std::string& json)
{
    auto j = nlohmann::json::parse(json);
    ParsedModel parsedModel = j.get<ParsedModel>();
    return from_parsed(parsedModel);
}

std::tuple<GeometricModel, UuidToModelKeyMap> from_parsed(const ParsedModel& parsed_model)
{
    UuidToModelKeyMap uuid_to_geometry;
    auto& uuid_to_node_key = uuid_to_geometry.nodeKeyMap;
    std::vector<Node> nodes;
    for(const auto& node : parsed_model.nodes)
    {
        nodes.push_back({node.x, node.y});
        uuid_to_node_key.insert({node.uuid, nodes.size() - 1});
    }


    auto& uuid_to_edge_key = uuid_to_geometry.edgeKeyMap;
    std::vector<Edge> edges;
    for(const auto& edge : parsed_model.edges)
    {
        NodeKey first_node;
        NodeKey second_node;
        if(const auto& key = uuid_to_node_key.find(edge.first_node); key != uuid_to_node_key.end())
        {
            first_node = key->second;
        }
        else
        {
            throw std::runtime_error("Edge refers to first node that doesn't exist");
        }

        if(const auto& key = uuid_to_node_key.find(edge.second_node); key != uuid_to_node_key.end())
        {
            second_node = key->second;
        }
        else
        {
            throw std::runtime_error("Edge refers to second node that doesn't exist");
        }
        edges.push_back({first_node, second_node});
        uuid_to_edge_key.insert({edge.uuid, edges.size() - 1});
    }

    Loops loops;
    for(const auto& loop : parsed_model.loops)
    {
        std::vector<EdgeKey> loop_edges;
        for (const auto &edge : loop.edges)
        {
            if (const auto &key = uuid_to_edge_key.find(edge); key != uuid_to_edge_key.end())
            {
                loop_edges.push_back(key->second);
            }
            else
            {
                throw std::runtime_error("Loop refers to edge that doesn't exist");
            }
        }
        loops.push_back(Loop{loop_edges});
    }


    return {GeometricModel{nodes, edges, loops}, uuid_to_geometry};
}

}