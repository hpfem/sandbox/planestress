#include "trace.hpp"

#include <algorithm>
#include <cmath>
#include <numbers>
#include <numeric>

namespace trace
{

namespace
{
    struct PointAndDirection
    {
        Point2D point;
        Point2D direction;
    };

    std::array<Point2D, 4> expand_cross(const Cross& cross)
    {
        std::array<Point2D, 4> result;

        for(int i = 0; i < 4; ++i)
        {
            auto dx = std::cos(cross.theta + i * std::numbers::pi / 2.);
            auto dy = std::sin(cross.theta + i * std::numbers::pi / 2.);

            result[i] = Point2D{dx, dy};
        }

        return result;
    }
    
    std::array<std::pair<Point2D, double>, 4> expand_eigendata(const EigenData& eigen_data)
    {
        std::array<std::pair<Point2D, double>, 4> result;

        result[0] = std::make_pair(Point2D{eigen_data.eigenvectors[0][0], eigen_data.eigenvectors[0][1]}, eigen_data.eigenvalues[0]);
        result[1] = std::make_pair(Point2D{eigen_data.eigenvectors[1][0], eigen_data.eigenvectors[1][1]}, eigen_data.eigenvalues[1]);
        result[2] = std::make_pair(Point2D{-eigen_data.eigenvectors[0][0], -eigen_data.eigenvectors[0][1]}, eigen_data.eigenvalues[0]);
        result[3] = std::make_pair(Point2D{-eigen_data.eigenvectors[1][0], -eigen_data.eigenvectors[1][1]}, eigen_data.eigenvalues[1]);

        return result;
    }


    double dot(const Point2D& a, const Point2D& b)
    {
        return a.x * b.x + a.y * b.y;
    }

    Point2D operator+(const Point2D& a, const Point2D& b)
    {
        return Point2D{a.x + b.x, a.y + b.y};
    }

    Point2D operator-(const Point2D& a, const Point2D& b)
    {
        return Point2D{a.x - b.x, a.y - b.y};
    }

    Point2D operator*(double a, const Point2D& b)
    {
        return Point2D{a * b.x, a * b.y};
    }

    // Given a cross and a direction, return the closest direction from the four
    // candidate directions that the cross represents
    Point2D closest_direction(const Cross& cross, const Point2D& direction)
    {
        auto expanded = expand_cross(cross);
        auto closest = std::max_element(expanded.begin(), expanded.end(), [&direction](const auto& dirA, const auto& dirB)
        {
            double dotA = dot(direction, dirA);
            double dotB = dot(direction, dirB);
            return dotA < dotB;
        });

        return *closest;
    }

    // Given an eigendata and a direction, return the eigenvector that points along the direction,
    // and the associated eigenvalue.
    std::pair<Point2D, double> closest_direction(const EigenData& eigen_data, const Point2D& direction)
    {
        auto expanded = expand_eigendata(eigen_data);
        auto closest = std::max_element(expanded.begin(), expanded.end(), [&direction](const auto& dirA, const auto& dirB)
        {
            double dotA = dot(direction, dirA.first);
            double dotB = dot(direction, dirB.first);
            return dotA < dotB;
        });

        return (*closest);
    }

}

TrajectoryBundle trace_trajectories(const CrossField& cross_field, const std::vector<Point2D>& seed_points)
{
    TraceSettings settings{
        .step_size = 0.001,
        .max_steps = 10000
    };

    return trace_trajectories(cross_field, seed_points, settings);
}
 
TrajectoryBundle trace_trajectories(const CrossField& cross_field, const std::vector<Point2D>& seed_points, const TraceSettings& trace_settings)
{
    std::vector<PointAndDirection> trajectory_seeds;
    trajectory_seeds.reserve(4 * seed_points.size());

    for(const auto& seed_point : seed_points)
    {
        const auto& cross = cross_field(seed_point);
        if(!cross.has_value())
        {
            continue;
        }

        for(const auto& direction : expand_cross(cross.value()))
        {
            trajectory_seeds.push_back({seed_point, direction});
        }
    }

    TrajectoryBundle result;

    for(auto [current_point, current_direction] : trajectory_seeds)
    {
        Trajectory current_trajectory;
        current_trajectory.points.push_back(current_point);
        for (int i_step = 0; i_step < trace_settings.max_steps; ++i_step)
        {
            auto next_point = current_point + trace_settings.step_size * current_direction;
            auto next_cross = cross_field(next_point);

            // If the next cross is nullopt, it means we are stepping outside
            // the domain, and we can terminate the trace
            if (!next_cross.has_value())
            {
                break;
            }

            auto next_direction = closest_direction(next_cross.value(), current_direction);

            current_direction = 0.5 * (current_direction + next_direction);
            current_point = current_point + trace_settings.step_size * current_direction;
            current_trajectory.points.push_back(current_point);
        }
        result.trajectories.push_back(current_trajectory);
    }

    return result;
}

TrajectoryBundle trace_trajectories(const EigenDataField& eigendata_field, const std::vector<Point2D>& seed_points, const TraceSettings& trace_settings)
{
    std::vector<std::pair<PointAndDirection, double>> trajectory_seeds;
    trajectory_seeds.reserve(4 * seed_points.size());

    for(const auto& seed_point : seed_points)
    {
        const auto& eigendata = eigendata_field(seed_point);
        if(!eigendata.has_value())
        {
            continue;
        }

        for(const auto& [direction, value] : expand_eigendata(eigendata.value()))
        {
            trajectory_seeds.push_back(std::make_pair(PointAndDirection{seed_point, direction}, value));
        }
    }

    TrajectoryBundle result;

    for(auto [current_point_and_direction, current_value] : trajectory_seeds)
    {
        Trajectory current_trajectory;
        auto [current_point, current_direction] = current_point_and_direction;
        current_trajectory.points.push_back(current_point);
        current_trajectory.values.push_back(current_value);
        for (int i_step = 0; i_step < trace_settings.max_steps; ++i_step)
        {
            auto next_point = current_point + trace_settings.step_size * current_direction;
            auto next_eigendata = eigendata_field(next_point);

            // If the next cross is nullopt, it means we are stepping outside
            // the domain, and we can terminate the trace
            if (!next_eigendata.has_value())
            {
                break;
            }

            auto next_direction = closest_direction(next_eigendata.value(), current_direction);

            current_direction = 0.5 * (current_direction + next_direction.first);
            current_point = current_point + trace_settings.step_size * current_direction;
            current_trajectory.points.push_back(current_point);

            // TODO (this is not entirely correct)
            current_trajectory.values.push_back(next_direction.second);
        }
        result.trajectories.push_back(current_trajectory);
    }

    return result;
}

EigenData eigenvectors(double S11, double S22, double S12)
{
    // Following https://people.math.harvard.edu/~knill/teaching/math21b2004/exhibits/2dmatrices/index.html
    constexpr double eps = 1E-12;
    double T = S11 + S22;
    double D = S11 * S22 - S12 * S12;

    std::array<std::array<double, 2>, 2> result{
        std::array<double, 2>{0.0, 0.0},
        std::array<double, 2>{0.0, 0.0}
        };

    if(std::fabs(S12) < eps)
    {
        result[0][0] = 1.0;
        result[1][1] = 1.0;
        return EigenData{
            .eigenvectors{result},
            .eigenvalues{S11, S22}
        };
    }

    double L1 = T / 2. + std::sqrt((T * T / 4. - D));
    double L2 = T / 2. - std::sqrt((T * T / 4. - D));

    result[0][0] = L1 - S22;
    result[0][1] = S12;
    double length_v1 = std::sqrt(std::inner_product(result[0].begin(), result[0].end(), result[0].begin(), 0.0));
    std::for_each(std::begin(result[0]), std::end(result[0]), [length_v1](auto& val){ val /= length_v1;});


    result[1][0] = L2 - S22;
    result[1][1] = S12;
    double length_v2 = std::sqrt(std::inner_product(result[1].begin(), result[1].end(), result[1].begin(), 0.0));
    std::for_each(std::begin(result[1]), std::end(result[1]), [length_v2](auto& val){ val /= length_v2;});

    return EigenData{
        .eigenvectors{result},
        .eigenvalues{L1, L2}
    };
}

} // namespace trace