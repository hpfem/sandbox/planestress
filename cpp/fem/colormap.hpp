#pragma once

#include "trace.hpp"

#include <map>
#include <string>
#include <vector>

namespace trace
{


std::map<std::string, std::string> map_to_jet(const TrajectoryBundle& trajectory_bundle);

}