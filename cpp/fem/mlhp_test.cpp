#include "mlhp_interface.hpp"

#include <catch2/catch_test_macros.hpp>

#include <fstream>
#include <iostream>


namespace
{

// Manufactured solution defined later
mlhp::spatial::VectorFunction<2, 2> solution ( );
mlhp::spatial::VectorFunction<2, 2> source( double E, double nu );
mlhp::spatial::VectorFunction<2, 3> stress( double E, double nu  );
mlhp::spatial::VectorFunction<2, 2> rightTraction( double E, double nu  );
mlhp::spatial::VectorFunction<2, 2> topTraction( double E, double nu  );

// Left, right, bottom top
std::array<std::vector<size_t>, 4> boundaryNodes( std::array<size_t, 2> nelements );

TEST_CASE( "CartesianGrid" )
{
    using namespace mlhp;

    auto nelements = std::array<size_t, 2> { 34, 42 };

    auto tmesh = trace::makeCartesianMesh<2>( nelements, { 0.0, 0.0 }, { 1.0, 1.0 } );
    auto analysis = trace::PlaneStressAnalysis { traceshared::to_json( std::move( tmesh ) ) };

    analysis.registerLogger( trace::makeConsoleLogger( ) ); 

    auto E = 2.3;
    auto nu = 0.3;
    
    auto [left, right, bottom, top] = boundaryNodes( nelements );

    // Dirichlet boundaries
    auto coordinates = [&]( size_t id ) { return analysis.mesh( ).vertex( id ); };
    auto analytical = solution( );

    for( auto id : left )
    {
        analysis.fixNode( id, 0, analytical( coordinates( id ) )[0] );
        analysis.fixNode( id, 1, analytical( coordinates( id ) )[1] );
    }
    
    for( auto id : bottom )
    {
        analysis.fixNode( id, 0, analytical( coordinates( id ) )[0] );
        analysis.fixNode( id, 1, analytical( coordinates( id ) )[1] );
    }
    
    // Neumann boundaries
    auto rtraction = rightTraction( E, nu );
    auto ttraction = topTraction( E, nu );

    for( size_t i = 0; i < right.size( ); ++i )
    {
        auto factor = i == 0 || i + 1 == right.size( ) ? 0.5 : 1.0;
        auto traction = rtraction( coordinates( right[i] ) );

        analysis.addNodalForce( right[i], factor / nelements[1] * traction );
    }

    for( size_t i = 0; i < top.size( ); ++i )
    {
        auto factor = i == 0 || i + 1 == top.size( ) ? 0.5 : 1.0;
        auto traction = ttraction( coordinates( top[i] ) );

        analysis.addNodalForce( top[i], factor / nelements[0] * traction );
    }

    // Body forces
    auto nallelements = analysis.basis( ).nelements( );
    auto mapping = analysis.mesh( ).createMapping( );
    auto bodyForce = source( E, nu );

    for( CellIndex ielement = 0; ielement < nallelements; ++ielement )
    {
        analysis.mesh( ).prepareMapping( ielement, mapping );

        for( size_t lvertex = 0; lvertex < 4; ++lvertex )
        {
            auto gvertex = analysis.mesh( ).vertexIndex( ielement, lvertex );
            auto force = bodyForce( coordinates( gvertex ) );

            analysis.addNodalForce( gvertex, mapping.detJ( { } ) * force );
        }
    }
    
    // Analysis
    analysis.setMaterialParameters( E, nu );

    analysis.initialize( );
    analysis.integrate( );
    analysis.solve( );
    analysis.finalize( );

    // Postprocessing
    analysis.writeVtu( "outputs/CartesianGrid.vtu" );
    
    auto expectedEnergy = std::sqrt( ( ( 117403.0 * nu - 1378393.0 ) * E ) / 
        ( 390.0 * ( 6930.0 * nu * nu - 6930.0 ) ) );
    
    auto computedEnergy = analysis.strainEnergy( );
    auto relativeError = std::abs( expectedEnergy - computedEnergy ) / expectedEnergy;

    CHECK( std::abs( relativeError - 0.0004409531 ) < 1e-5 );
}

// Manufactured solution
mlhp::spatial::VectorFunction<2, 2> solution( )
{
    return [=] ( std::array<double, 2> xy ) 
    {
        return std::array { std::pow( xy[0], 2 ) * std::pow( xy[1], 3 ),
                            std::pow( xy[0], 5 ) * std::pow( xy[1], 7 ) };
    };
}

// Corresponding right hand side
mlhp::spatial::VectorFunction<2, 2> source( double E, double nu )
{
    return [=]( std::array<double, 2> xy ) noexcept
    {
        double fx = - ( ( 1.0 - nu ) * ( 35.0 * std::pow( xy[0], 4 ) * std::pow( xy[1], 6 ) + 6.0 * 
            std::pow( xy[0], 2 ) * xy[1] ) * E ) / ( 2.0 * ( 1.0 - nu * nu ) ) - ( 35.0 * nu * 
            std::pow( xy[0], 4 ) * std::pow( xy[1], 6 ) * E ) / ( 1.0 - nu * nu ) - 
            ( 2.0 * std::pow( xy[1], 3 ) * E ) / ( 1.0 - nu * nu );

        double fy = - ( ( 1.0 - nu ) * ( 20.0 * std::pow( xy[0], 3 ) * std::pow( xy[1], 7 ) + 6.0 * 
            std::pow( xy[1], 2 ) * xy[0] ) * E ) / ( 2.0 * ( 1.0 - nu * nu ) ) - ( 42.0 * 
            std::pow( xy[0], 5 ) * std::pow( xy[1], 5 ) * E ) / ( 1.0 - nu * nu ) - 
            ( 6.0 * nu * xy[0] * std::pow( xy[1], 2 ) * E ) / ( 1.0 - nu * nu );

        return std::array { fx, fy };
    };
}

// Corresponding stress tensor
mlhp::spatial::VectorFunction<2, 3> stress( double E, double nu  )
{
    return [=]( std::array<double, 2> xy ) noexcept
    {
        auto S = std::array<double, 3> { };
        auto c = E / ( 1.0 - nu * nu );
        
        auto xy3 = xy[0] * std::pow( xy[1], 3 );
        auto x4y3 = std::pow( xy[0], 4 ) * std::pow( xy[1], 3 );
        auto x2y2 = std::pow( xy[0], 2 ) * std::pow( xy[1], 2 );
        auto x2y5 = std::pow( xy[0], 2 ) * std::pow( xy[1], 5 );
        
        S[0] = c * xy3 * ( 2.0 + 7.0 * nu * x4y3 );
        S[1] = c * xy3 * ( 2.0 * nu + 7.0 * x4y3 );
        S[2] = 0.5 * c * ( 1.0 - nu ) * x2y2 * ( 3.0 + 5.0 * x2y5 );
        
        return S;
    };
}

// Extract [s_xy, s_yy] of stress tensor
mlhp::spatial::VectorFunction<2, 2> topTraction( double E, double nu  )
{
    auto S = stress( E, nu );

    return [=]( std::array<double, 2> xy )
    { 
        return std::array { S( xy )[2], S( xy )[1] };
    };
}

// Extract [s_xx, s_xy] of stress tensor
mlhp::spatial::VectorFunction<2, 2> rightTraction( double E, double nu  )
{
    auto S = stress( E, nu );

    return [=]( std::array<double, 2> xy )
    { 
        return std::array { S( xy )[0], S( xy )[2] };
    };
}

std::array<std::vector<size_t>, 4> boundaryNodes( std::array<size_t, 2> nelements )
{
    auto result = std::array<std::vector<size_t>, 4> { };
    auto nvertices = mlhp::array::add( nelements, size_t { 1 } );

    for( size_t i = 0; i <= nelements[0]; ++i )
    {
        for( size_t j = 0; j <= nelements[1]; ++j )
        {
            auto index = i * ( nelements[1] + 1 ) + j;
            
            if( i == 0 ) result[0].push_back( index );
            if( j == 0 ) result[2].push_back( index );
            if( i == nelements[0] ) result[1].push_back( index );
            if( j == nelements[1] ) result[3].push_back( index );
        }
    }

    return result;
}

} // test1
