#include "trace.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <cmath>
#include <numbers>
#include <fstream>
#include <iostream>

namespace
{

std::ostream& operator<<(std::ostream& ostr, const trace::Point2D& point)
{
    ostr << point.x << " " << point.y;
    return ostr;
}

class Point2DEqualMatcher : public Catch::Matchers::MatcherGenericBase
{
    public:
    Point2DEqualMatcher(const trace::Point2D& point) : point_{point} {}

    bool match(const trace::Point2D& other) const
    {
        return Catch::Matchers::WithinAbs(point_.x, 1E-7).match(other.x) && Catch::Matchers::WithinAbs(point_.y, 1E-7).match(other.y);
    }

    std::string describe() const override
    {
        return "Point2D equals: " + std::to_string(point_.x) + " " + std::to_string(point_.y);
    }

    private:
    const trace::Point2D& point_;
};

}

TEST_CASE("trace empty trajectory")
{
    std::vector<trace::Point2D> seeds;

    auto cross_field = [](const trace::Point2D& p)
    {
        return trace::Cross{0.0};
    };

    auto result = trace::trace_trajectories(cross_field, seeds);

    CHECK(result.trajectories.size() == 0);
}

TEST_CASE("trace a trajectory on a circular eigenvector field")
{
    auto circular_eigenvector_field = [](const trace::Point2D& query) -> std::optional<trace::EigenData>
    {
        if(std::fabs(query.x) < 1E-7)
        {
            return std::nullopt;
        }

        double angle = std::atan(query.y / query.x );

        if(angle < 0.0)
        {
            angle += std::numbers::pi / 2.;
        }

        // Circular eigenvector field where the radial direction has eigenvalue
        // 1.0 and the circumferential direction has eigenvalue 2.0
        return trace::EigenData{
            .eigenvectors{
                std::array<double, 2>{std::cos(angle), std::sin(angle)},
                std::array<double, 2>{-std::sin(angle), std::cos(angle)}
            },
            .eigenvalues{1.0, 2.0}
        };
    };

    trace::Point2D seed_point{5.0, 0.0};

    trace::TraceSettings settings{
        .step_size = 1E-3,
        .max_steps = 100
    };

    auto linear_length = settings.max_steps * settings.step_size;

    auto result = trace::trace_trajectories(circular_eigenvector_field, std::vector<trace::Point2D>{seed_point}, settings);

    CHECK(result.trajectories.size() == 4);

    // This is not very nice, because we rely on an implementation detail here:
    // The first and third trajectories go along the radius, while the second
    // and fourth go in the circumferential direction. This is because "expand
    // cross" expands it this way
    CHECK(result.trajectories[0].points.size() == 101);
    CHECK(result.trajectories[1].points.size() == 101);
    CHECK(result.trajectories[2].points.size() == 101);
    CHECK(result.trajectories[3].points.size() == 101);

    CHECK(result.trajectories[0].values.size() == 101);
    CHECK(result.trajectories[1].values.size() == 101);
    CHECK(result.trajectories[2].values.size() == 101);
    CHECK(result.trajectories[3].values.size() == 101);

    CHECK_THAT(result.trajectories[0].points[0], Point2DEqualMatcher(seed_point));
    CHECK_THAT(result.trajectories[1].points[0], Point2DEqualMatcher(seed_point));
    CHECK_THAT(result.trajectories[2].points[0], Point2DEqualMatcher(seed_point));
    CHECK_THAT(result.trajectories[3].points[0], Point2DEqualMatcher(seed_point));
    
    CHECK_THAT(result.trajectories[0].values[0], Catch::Matchers::WithinRel(1.0));
    CHECK_THAT(result.trajectories[1].values[0], Catch::Matchers::WithinRel(2.0));
    CHECK_THAT(result.trajectories[2].values[0], Catch::Matchers::WithinRel(1.0));
    CHECK_THAT(result.trajectories[3].values[0], Catch::Matchers::WithinRel(2.0));

    CHECK_THAT(result.trajectories[0].points[100], Point2DEqualMatcher(trace::Point2D{seed_point.x + linear_length, seed_point.y }));
    CHECK_THAT(result.trajectories[2].points[100], Point2DEqualMatcher(trace::Point2D{seed_point.x - linear_length, seed_point.y }));

    auto last_point_circumferential = result.trajectories[1].points[100];

    auto magnitude = [](const trace::Point2D& p)
    {
        return std::sqrt(p.x * p.x + p.y * p.y);
    };

    CHECK_THAT(magnitude(last_point_circumferential), Catch::Matchers::WithinAbs(5.0, 1E-2));

    last_point_circumferential = result.trajectories[3].points[100];
    CHECK_THAT(magnitude(last_point_circumferential), Catch::Matchers::WithinAbs(5.0, 1E-2));
}

TEST_CASE("trace a trajectory on a ring-like cross field")
{
    constexpr double min_radius = 1.0;
    constexpr double max_radius = 2.0;
    constexpr double min_theta = 0.0;
    constexpr double max_theta = std::numbers::pi / 2.;

    auto cross_field = [=](const trace::Point2D& query) -> std::optional<trace::Cross>
    {
        if((query.x * query.x + query.y * query.y) > (max_radius * max_radius) ||
           (query.x * query.x + query.y * query.y) < (min_radius * min_radius))
           {
            return std::nullopt;
           }

        double angle = std::atan2(query.y, query.x );

        if(angle > max_theta || angle < min_theta)
        {
            return std::nullopt;
        }

        return trace::Cross{angle};
    };
}

TEST_CASE("Eigenvectors")
{
    double S11 = 0.0;
    double S22 = 0.0;
    double S12 = 0.17029935;

    trace::EigenData eigendata = trace::eigenvectors(S11, S22, S12);

    constexpr double sqrt2_2 = std::numbers::sqrt2 / 2.0;

    const auto& close = [](const auto& a, const auto& b)
    {
        return std::fabs(a - b) < 1E-12;
    };

    CHECK_THAT(eigendata.eigenvectors[0], Catch::Matchers::RangeEquals(std::array<double, 2>{sqrt2_2, sqrt2_2}, close));
    CHECK_THAT(eigendata.eigenvectors[1], Catch::Matchers::RangeEquals(std::array<double, 2>{-sqrt2_2, sqrt2_2}, close));

    CHECK_THAT(eigendata.eigenvalues[0], Catch::Matchers::WithinRel(S12));
    CHECK_THAT(eigendata.eigenvalues[1], Catch::Matchers::WithinRel(-S12));

    S11 = 1.0;
    S22 = 2.0;
    S12 = 0.0;

    eigendata = trace::eigenvectors(S11, S22, S12);
    CHECK_THAT(eigendata.eigenvectors[0], Catch::Matchers::RangeEquals(std::array<double, 2>{1.0, 0.0}, close));
    CHECK_THAT(eigendata.eigenvectors[1], Catch::Matchers::RangeEquals(std::array<double, 2>{0.0, 1.0}, close));

    CHECK_THAT(eigendata.eigenvalues[0], Catch::Matchers::WithinRel(S11));
    CHECK_THAT(eigendata.eigenvalues[1], Catch::Matchers::WithinRel(S22));

    S11 = 1.0;
    S22 = 1.0;
    S12 = 2.0;

    eigendata = trace::eigenvectors(S11, S22, S12);
    CHECK_THAT(eigendata.eigenvectors[0], Catch::Matchers::RangeEquals(std::array<double, 2>{sqrt2_2, sqrt2_2}, close));
    CHECK_THAT(eigendata.eigenvectors[1], Catch::Matchers::RangeEquals(std::array<double, 2>{-sqrt2_2, sqrt2_2}, close));

    CHECK_THAT(eigendata.eigenvalues[0], Catch::Matchers::WithinRel(3.0));
    CHECK_THAT(eigendata.eigenvalues[1], Catch::Matchers::WithinRel(-1.0));
}