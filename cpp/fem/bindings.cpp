#include "trace.hpp"
#include "mlhp_interface.hpp"

#include <emscripten/bind.h>
//#include <sanitizer/lsan_interface.h>

namespace
{

// Wrap the result of the simulation (which is just an std::function) into a
// class, so that we can handle it on the JS side easier. The other alternative would
// be to bind functors as it is done in the embind tests (see the part with opcall in embind_test.cpp)
struct SimulationResult
{
    trace::CrossField cross_field_;
    trace::TrajectoryBundle trace_trajectories(const std::vector<trace::Point2D>& seed_points)
    {
        // Cross field should never be zero, because JS has only access to the constructor that initializes it
        return trace::trace_trajectories(cross_field_, seed_points);
    }
};

}

EMSCRIPTEN_BINDINGS(emtrace)
{
    emscripten::class_<trace::TrajectoryBundle>("TrajectoryBundle")
    .property("trajectories", &trace::TrajectoryBundle::trajectories);

    emscripten::class_<trace::Trajectory>("Trajectory")
    .property("points", &trace::Trajectory::points);

    emscripten::class_<trace::Point2D>("Point2D")
    .constructor()
    .property("x", &trace::Point2D::x)
    .property("y", &trace::Point2D::y);

    emscripten::class_<trace::PlaneStressAnalysis>("PlaneStressAnalysis")
    .constructor<const std::string&>()
    .function("setMaterialParameters", &trace::PlaneStressAnalysis::setMaterialParameters)
    .function("fixNode", &trace::PlaneStressAnalysis::fixNode)
    .function("setBodyForce", emscripten::select_overload<void(trace::PlaneStressAnalysis&, double, double)>([](trace::PlaneStressAnalysis& instance, double fx, double fy)
     {
        instance.setBodyForce({fx, fy});
     }))
    .function("solve", emscripten::select_overload<void(trace::PlaneStressAnalysis&)>([](trace::PlaneStressAnalysis& instance)
    {
        instance.initialize();
        instance.integrate();
        instance.solve();
        instance.finalize();
    }))
    .function("addTrace", emscripten::select_overload<void(trace::Point2D)>(&trace::PlaneStressAnalysis::addTrace))
    .function("clearTraces", &trace::PlaneStressAnalysis::clearTraces)
    .function("cachedTrajectories", &trace::PlaneStressAnalysis::cachedTrajectories)
    .function("svgTrajectories", &trace::PlaneStressAnalysis::svgTrajectories)
    .function("turnOnLogger", emscripten::select_overload<void(trace::PlaneStressAnalysis&)>([](trace::PlaneStressAnalysis& instance){
        instance.registerLogger([](const std::string& msg){std::cout << msg << std::endl;});
    }));

    emscripten::register_vector<trace::Trajectory>("TrajectoryVector");
    emscripten::register_vector<trace::Point2D>("Point2DVector");
//    emscripten::function("doLeakCheck", &__lsan_do_recoverable_leak_check);
}