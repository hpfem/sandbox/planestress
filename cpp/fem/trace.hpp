#pragma once

#include <functional>
#include <optional>
#include <vector>
#include <cstddef>
#include <array>

namespace trace
{

struct Point2D
{
    double x;
    double y;
};

struct Trajectory
{
    std::vector<Point2D> points;
    std::vector<double> values;
};

// A set of trajectories that the tracer returns
struct TrajectoryBundle
{
    std::vector<Trajectory> trajectories;
};

// A single cross - it only has an angle that lies between 0 and pi / 2
struct Cross
{
    double theta;
};

// Struct that holds the eigenvectors and the associated eigenvalues
struct EigenData
{
    std::array<std::array<double, 2>, 2> eigenvectors;
    std::array<double, 2> eigenvalues;
};

struct TraceSettings
{
    double step_size;
    size_t max_steps;
};

// Given a point in space, return the cross that belongs to this point
using CrossField = std::function<std::optional<Cross>(Point2D)>;
using EigenDataField = std::function<std::optional<EigenData>(Point2D)>;

TrajectoryBundle trace_trajectories(const CrossField& cross_field, const std::vector<Point2D>& seed_points);
TrajectoryBundle trace_trajectories(const CrossField& cross_field, const std::vector<Point2D>& seed_points, const TraceSettings& trace_settings);
TrajectoryBundle trace_trajectories(const EigenDataField& eigendata_field, const std::vector<Point2D>& seed_points, const TraceSettings& trace_settings);

EigenData eigenvectors(double S11, double S22, double S12);

}