#include "colormap.hpp"

#include "fmt/format.h"

namespace trace
{

    namespace
    {

        constexpr std::array jet_cmap = {
            "#000080",
            "#000092",
            "#0000a4",
            "#0000b7",
            "#0000c9",
            "#0000db",
            "#0000ee",
            "#0000ff",
            "#0002ff",
            "#0012ff",
            "#0022ff",
            "#0033ff",
            "#0043ff",
            "#0053ff",
            "#0063ff",
            "#0073ff",
            "#0084ff",
            "#0094ff",
            "#00a4ff",
            "#00b4ff",
            "#00c4ff",
            "#00d4ff",
            "#00e5f7",
            "#0cf5ea",
            "#19ffdd",
            "#27ffd0",
            "#34ffc3",
            "#41ffb6",
            "#4effa9",
            "#5bff9c",
            "#68ff8f",
            "#75ff82",
            "#82ff75",
            "#8fff68",
            "#9cff5b",
            "#a9ff4e",
            "#b6ff41",
            "#c3ff34",
            "#d0ff27",
            "#ddff19",
            "#eaff0c",
            "#f7f500",
            "#ffe600",
            "#ffd700",
            "#ffc800",
            "#ffb900",
            "#ffaa00",
            "#ff9b00",
            "#ff8c00",
            "#ff7d00",
            "#ff6e00",
            "#ff5f00",
            "#ff5000",
            "#ff4100",
            "#ff3200",
            "#ff2300",
            "#ff1400",
            "#ee0500",
            "#db0000",
            "#c90000",
            "#b70000",
            "#a40000",
            "#920000",
            "#800000"};

    }

    std::map<std::string, std::string> map_to_jet(const TrajectoryBundle &trajectory_bundle)
    {
        std::map<std::string, std::string> result;
        for(const auto& color : jet_cmap)
        {
            result[color] = {};
        }
        auto min = std::numeric_limits<double>::max();
        auto max = std::numeric_limits<double>::min();
        for (const auto &trajectory : trajectory_bundle.trajectories)
        {
            if (trajectory.points.size() != trajectory.values.size())
            {
                throw std::runtime_error("Not every point in trajectory has an associated value!");
            }
            for (size_t i = 0; i < trajectory.points.size(); ++i)
            {
                min = std::min(min, trajectory.values[i]);
                max = std::max(max, trajectory.values[i]);
            }
        }

        constexpr auto colormapSize = jet_cmap.size();

        for (const auto &trajectory : trajectory_bundle.trajectories)
        {
            for (size_t i = 1; i < trajectory.points.size(); ++i)
            {
                auto value = trajectory.values[i - 1];
                auto start_point = trajectory.points[i - 1];
                auto end_point = trajectory.points[i];
                int index = std::min<int>(std::max<int>(0, static_cast<int>(std::round((value - min) * (colormapSize - 1) / (max - min)))), colormapSize - 1);
                auto color = jet_cmap[index];
                result[color] += fmt::format("M{} {}L{} {}", start_point.x, start_point.y, end_point.x, end_point.y);
            }
        }
        return result;
    }
} // namespace trace