#pragma once

#include "trace.hpp"
#include "mesh.hpp"
#include "mlhp/core.hpp"

namespace trace
{


using Logger = std::function<void( const std::string& msg )>;

Logger makeConsoleLogger( );

class PlaneStressAnalysis 
{
public:

    // Preprocessing / setup
    PlaneStressAnalysis( const std::string& meshJson );

    void setMaterialParameters( double youngsModulus, double poissonRatio );

    bool fixNode( size_t nodeIndex, size_t fieldIndex, double value = 0.0 );
    void setBodyForce( std::array<double, 2> force );
    bool addNodalForce( size_t nodeIndex, std::array<double, 2> traction );
    
    const mlhp::UnstructuredMesh<2>& mesh( ) const { return *meshPtr; }
    const mlhp::UnstructuredBasis<2>& basis( ) const { return *basisPtr; }

    // Finite element analysis
    void initialize( );
    void integrate( );
    void solve( );
    void finalize( );

    // Solution postprocessing
    void addTrace( Point2D seed );
    void addTrace( Point2D seed, TraceSettings settings );
    void clearTraces();
    TrajectoryBundle cachedTrajectories( ) const; // replace by svg?

    std::string svgTrajectories() const;

    void writeVtu( std::string file ) const;

    double strainEnergy( ) const;

    // Utilities
    void registerLogger( const Logger& logger );

private:
    std::vector<Logger> loggers;

    void log( std::string&& msg ) const;

    // Plane stress model
    mlhp::Constitutive<2> material;
    mlhp::spatial::VectorFunction<2> bodyForce;

    // Boundary conditions
    using NodalForce = std::pair<size_t, std::array<double, 2>>;

    mlhp::DofIndicesValuesPair dirichlet;
    std::vector<NodalForce> nodalForces;

    // Discretization    
    std::shared_ptr<mlhp::UnstructuredMesh<2>> meshPtr;
    std::shared_ptr<mlhp::UnstructuredBasis<2>> basisPtr;
    std::vector<size_t> indexmap;

    // Linear solution
    mlhp::linalg::SymmetricSparseMatrix K;
    std::vector<double> F;
    std::vector<double> u;

    // Postprocessing 
    struct EvaluationCache;

    std::shared_ptr<EvaluationCache> cache;

    EigenDataField eigenDataField;
    TrajectoryBundle trajectories;
};

template<size_t D>
traceshared::Mesh makeCartesianMesh( std::array<size_t, D> nelements, 
                                     std::array<double, D> origin,
                                     std::array<double, D> lengths );

}