// License: See LICENSE

#include "mlhp_interface.hpp"
#include "colormap.hpp"

#include "mlhp/core.hpp"

#include "nlohmann/json.hpp"

#include <ranges>

namespace
{

    using namespace mlhp;

    using CrossField = std::function<std::optional<double>(std::array<double, 2>)>;

    CrossField makeEvaluator(std::shared_ptr<AbsBasis<2>> basis,
                             const Kinematics<2> &kinematics,
                             const Constitutive<2> &constitutive,
                             const std::vector<double> &dofs)
    {
        static constexpr size_t D = 2;
        static constexpr size_t ncomponents = (D * (D + 1)) / 2;

        // Since the tmp stuff below must exist for each thread separately (as thread_local for example)
        MLHP_CHECK(parallel::getMaxNumberOfThreads() == 1, "Doesn't work with multiple threads.");

        auto tmp = std::tuple{
            std::shared_ptr{basis->mesh().createBackwardMapping()},
            basis->createEvaluationCache(),
            BasisFunctionEvaluation<2>(),
            std::vector<DofIndex>{}};

        return [basis, dofs, kinematics, constitutive, tmp = std::move(tmp)](std::array<double, D> xy) mutable -> std::optional<double>
        {
            auto &[backwardMapping, basisCache, shapes, locationMap] = tmp;

            if (auto result = backwardMapping->map(xy); result)
            {
                auto [icell, rs] = *result;

                utilities::resize0(locationMap);

                // Evaluate shape functions
                basis->prepareEvaluation(icell, 1, shapes, basisCache);
                basis->evaluateSinglePoint(rs, shapes, basisCache);
                basis->locationMap(icell, locationMap);

                // Evaluate deformation gradient, strains, and then stresses
                auto gradient = std::array<double, D * D>{};
                auto strain = std::array<double, ncomponents>{};
                auto stress = std::array<double, ncomponents>{};

                evaluateSolutions(shapes, locationMap, dofs, gradient, 1);

                kinematics.evaluate(shapes, gradient, std::span{strain}, std::span<double>{});
                constitutive.evaluate(shapes, std::span{strain}, std::span{stress}, 1);

                // Compute principal stress directions
                auto [S11, S22, S12] = stress;
                const auto& [eigenvectors, eigenvalues] = trace::eigenvectors(S11, S22, S12);

                auto v0 = eigenvectors[0];
                if (std::fabs(v0[0]) < 1E-12)
                {
                    return std::numbers::pi / 2.;
                }

                double angle = std::atan(v0[1] / v0[0]);
                if (angle < 0.0)
                {
                    angle += std::numbers::pi / 2.;
                }

                return angle;
            }

            return std::nullopt;
        };
    }

    CrossField compute_internal()
    {
        static constexpr size_t D = 2;

        auto mesh = makeRefinedGrid<D>({100, 25}, {2.0, 0.5});
        auto basis = makeHpBasis<TrunkSpace>(mesh, size_t{1}, D);

        // Physics
        auto E = spatial::constantFunction<D>(1.0);
        auto nu = spatial::constantFunction<D>(0.3);

        auto f = spatial::constantFunction<D>(std::array{0.0, -1.0});

        auto constitutive = makePlaneStressMaterial(E, nu);
        auto kinematics = makeSmallStrainKinematics<D>();
        auto integrand = makeIntegrand(kinematics, constitutive, f);

        // Dirichlet boundary conditions
        auto time0 = utilities::tic();
        auto zero = spatial::constantFunction<D>(0.0);

        auto dirichletComponents = std::vector{
            boundary::boundaryDofs(zero, *basis, {boundary::left}, 0),
            boundary::boundaryDofs(zero, *basis, {boundary::left}, 1),
            boundary::boundaryDofs(zero, *basis, {boundary::right}, 0),
            boundary::boundaryDofs(zero, *basis, {boundary::right}, 1)};

        auto dirichlet = boundary::combine(std::move(dirichletComponents));

        auto time1 = utilities::tic();

        // Assemble
        auto K = allocateMatrix<linalg::SymmetricSparseMatrix>(*basis, dirichlet.first);
        auto F = std::vector<double>(K.size1(), 0.0);

        auto time2 = utilities::tic();

        integrateOnDomain<D>(*basis, integrand, {K, F}, dirichlet);

        // Solve
        auto time3 = utilities::tic();

        auto [solver, info] = linalg::makeCGSolverWithInfo();

        auto dofs = boundary::inflate(solver(K, F), dirichlet);

        // Print stuff
        auto time4 = utilities::tic();

        print(*mesh, std::cout << std::endl);
        print(*basis, std::cout << std::endl);
        print(K, std::cout << std::endl);

        std::cout << "\nFound " << dirichlet.first.size() << " dirichlet dofs." << std::endl;
        std::cout << "Linear solution converged in " << info->niterations() << " CG iterations." << std::endl;

        std::cout << "\nTotal time: " << 1000.0 * utilities::seconds(time0, time4) << " ms, with \n"
                  << "    boundary dofs     : " << 1000.0 * utilities::seconds(time0, time1) << " ms\n"
                  << "    matrix allocation : " << 1000.0 * utilities::seconds(time1, time2) << " ms\n"
                  << "    assembly          : " << 1000.0 * utilities::seconds(time2, time3) << " ms\n"
                  << "    linear solution   : " << 1000.0 * utilities::seconds(time3, time4) << " ms" << std::endl;

        // Create evaluator
        auto evaluator = makeEvaluator(basis, kinematics, constitutive, dofs);

        // Write .vtu file (remove this if you don't need it)
        // auto evaluatorWrapper = std::function([&evaluator](std::array<double, 2> xy)
        //                                      {
        // auto direction = std::array { 0.0, 0.0 };

        // if( auto angle = evaluator( xy ); angle )
        //{
        //     direction[0] = std::cos( *angle );
        //     direction[1] = std::sin( *angle );
        // }

        // return direction; });

        // auto processors = std::tuple{
        //     makeSolutionProcessor<D>(dofs),
        //     makeFunctionProcessor<D>(f, "Source"),
        //     makeFunctionProcessor<D>(evaluatorWrapper, "PrincipalStressDirection"),
        //     makeVonMisesProcessor<D>(dofs, kinematics, constitutive)};

        // auto nsamples = array::make<D>(size_t{1});
        // auto postmesh = createGridOnCells(nsamples);
        // auto file = "outputs/cartesian";

        // writeOutput(*basis, postmesh, std::move(processors), VtuOutput{file});

        return evaluator;
    }
}


namespace trace
{

Logger makeConsoleLogger( )
{
    return []( const std::string& msg )
    {
        std::cout << msg << std::flush;
    };
}

namespace
{

auto convertToMlhpMesh( auto&& mesh )
{
    auto vertices = mlhp::CoordinateList<2>( mesh.coordinates.size( ) / 2 );

    for( size_t i = 0; i < vertices.size( ); ++i )
    {
        vertices[i] = { mesh.coordinates[2 * i], mesh.coordinates[2 * i + 1] };
    }

    mesh.offsets.push_back( mesh.connectivities.size( ) );

    auto indexmap = topology::filterVertices( vertices, mesh.connectivities );

    return std::tuple { std::make_shared<mlhp::UnstructuredMesh<2>>( std::move( vertices ), 
        std::move( mesh.connectivities ), std::move( mesh.offsets ) ), indexmap };
}

} // namespace

PlaneStressAnalysis::PlaneStressAnalysis( const std::string& json )
{ 
    std::tie( meshPtr, indexmap ) = convertToMlhpMesh( traceshared::from_json( json ) );
    
    basisPtr = std::make_shared<mlhp::UnstructuredBasis<2>>( meshPtr, 2 );

    setMaterialParameters( 1.0, 0.3 );
    setBodyForce( { 0.0, 0.0 } );
}

void PlaneStressAnalysis::setMaterialParameters( double E, double nu )
{
    auto constantE = spatial::constantFunction<2>( E );
    auto constantNu = spatial::constantFunction<2>( nu );

    material = makePlaneStressMaterial( constantE, constantNu );
}

bool PlaneStressAnalysis::fixNode( size_t nodeIndex, size_t fieldIndex, double value )
{
    auto exists = nodeIndex < indexmap.size( ) && fieldIndex < 2 && 
        indexmap[nodeIndex] != NoValue<size_t> ;

    if( exists )
    {
        auto dofIndex = 2 * indexmap[nodeIndex] + fieldIndex;

        dirichlet.first.push_back( static_cast<DofIndex>( dofIndex ) );
        dirichlet.second.push_back( value );
    }

    return exists;
}

void PlaneStressAnalysis::setBodyForce( std::array<double, 2> force )
{
    bodyForce = spatial::constantFunction<2>( force );
}

bool PlaneStressAnalysis::addNodalForce( size_t nodeIndex, std::array<double, 2> traction )
{
    auto exists = nodeIndex < indexmap.size( ) && 
        indexmap[nodeIndex] != NoValue<size_t>;
        
    if( exists )
    {
        nodalForces.push_back( { indexmap[nodeIndex], traction } );
    }

    return exists;
}

void PlaneStressAnalysis::initialize( )
{
    log( "Starting plane stress analysis\n" );
    log( "    number of elements      : " + std::to_string( basisPtr->nelements( ) ) + "\n" );
    log( "    number of unknowns      : " + std::to_string( basisPtr->ndof( ) )      + "\n" );
    log( "    mesh heap memory usage  : " + utilities::memoryUsageString( meshPtr->memoryUsage( ) ) + "\n" );

    dirichlet = mlhp::boundary::makeUnique( dirichlet );
    
    log( "Allocating sparse matrix\n" );

    K = mlhp::allocateMatrix<linalg::SymmetricSparseMatrix>( *basisPtr, dirichlet.first );
    F = std::vector<double>( basisPtr->ndof( ), 0.0 );
    
    auto fillRatio = ( 2.0 * K.nnz( ) - K.size1( ) ) / ( K.size1( ) * K.size2( ) );
    log( "    number of nonzeros      : " + std::to_string( K.nnz( ) ) + "\n");
    log( "    fill ratio              : " + std::to_string( 100.0 * fillRatio ) + "%\n");

    for( auto [id, traction] : nodalForces )
    {
        F[2 * id + 0] += traction[0];
        F[2 * id + 1] += traction[1];
    }

    F = mlhp::algorithm::remove( F, dirichlet.first );
}

void PlaneStressAnalysis::integrate( )
{
    log( "Assembling finite element contributions\n" );

    auto kinematics = makeSmallStrainKinematics<2>( );
    auto integrand = makeIntegrand( kinematics, material, bodyForce );

    integrateOnDomain<2>( *basisPtr, integrand, { K, F }, dirichlet );
}

void PlaneStressAnalysis::solve( )
{
    log( "Solving sparse linear system\n" );

    auto [solver, info] = mlhp::linalg::makeCGSolverWithInfo( );

    u = mlhp::boundary::inflate( solver( K, F ), dirichlet );
    
    log( "    number of CG iterations : " + std::to_string( info->niterations( ) ) + "\n");
}

struct PlaneStressAnalysis::EvaluationCache
{
    std::shared_ptr<AbsBackwardMapping<2>> mapping;
    BasisFunctionEvaluation<2> shapes;
    BasisEvaluationCache<2> basisCache;
    std::vector<DofIndex> locationMap;
};

void PlaneStressAnalysis::finalize( )
{
    auto emptyK = mlhp::linalg::SymmetricSparseMatrix { };
    auto emptyF = std::vector<double> { };

    std::swap( K, emptyK );
    std::swap( F, emptyF );
    
    log( "Creating kdtree for backward mapping\n" );

    cache = std::make_shared<EvaluationCache>( );
    cache->mapping = meshPtr->createBackwardMapping( );
    cache->basisCache = basisPtr->createEvaluationCache( );
    
    static constexpr size_t D = 2;
    static constexpr size_t ncomponents = ( D * ( D +  1 ) ) / 2;

    MLHP_CHECK( parallel::getMaxNumberOfThreads( ) == 1, "Concurrent "
        "write access to cached data in PlaneStressAnalysis won't work." );

    auto kinematics = makeSmallStrainKinematics<2>( );

    eigenDataField = [=, this]( Point2D xy ) -> std::optional<EigenData>
    {
        if( auto result = cache->mapping->map( { xy.x, xy.y } ); result )
        {
            auto [icell, rs] = *result;

            utilities::resize0( cache->locationMap );

            // Evaluate shape functions
            basisPtr->prepareEvaluation( icell, 1, cache->shapes, cache->basisCache );
            basisPtr->evaluateSinglePoint( rs, cache->shapes, cache->basisCache );
            basisPtr->locationMap( icell, cache->locationMap );

            // Evaluate deformation gradient, strains, and then stresses
            auto gradient = std::array<double, D * D> { };
            auto strain = std::array<double, ncomponents> { };
            auto stress = std::array<double, ncomponents> { };

            evaluateSolutions( cache->shapes, cache->locationMap, u, gradient, 1 );

            kinematics.evaluate( cache->shapes, gradient, std::span { strain }, std::span<double> { } );
            material.evaluate( cache->shapes, std::span { strain }, std::span { stress }, 1 );

            // Compute principal stress directions
            auto [S11, S22, S12] = stress;
            return trace::eigenvectors(S11, S22, S12);
        }

        return std::nullopt;
    };

    log( "Finished plane stress analysis.\n" );
}

void PlaneStressAnalysis::addTrace( Point2D seed )
{
    addTrace( seed, { .step_size = 0.1, .max_steps = 10000 } );
}

void PlaneStressAnalysis::addTrace( Point2D seed, TraceSettings settings )
{
    auto bundle = trace::trace_trajectories( eigenDataField, { seed }, settings );

    std::for_each(bundle.trajectories.begin(), bundle.trajectories.end(), [&](const auto& trajectory){trajectories.trajectories.push_back(trajectory);});
}

void PlaneStressAnalysis::clearTraces()
{
    trajectories.trajectories.clear();
}

TrajectoryBundle PlaneStressAnalysis::cachedTrajectories( ) const
{
    return trajectories;
}
    
std::string PlaneStressAnalysis::svgTrajectories() const
{
    auto coloredSvgSegments = map_to_jet(trajectories);

    nlohmann::json j;

    for(const auto& [key, value] : coloredSvgSegments)
    {
        j[key] = value;
    }

    std::stringstream ss;
    ss << j;
    return ss.str();
}

void PlaneStressAnalysis::writeVtu( std::string file ) const
{
    static constexpr size_t D = 2;
    
    log( "Writing vtu file ... " );

    auto kinematics = mlhp::makeSmallStrainKinematics<D>( );
    auto resolution = array::make<D>( size_t { 1 } );
    auto postmesh = mlhp::createGridOnCells( resolution );
    auto output = VtuOutput { file };

    auto processors = std::tuple
    {
        makeSolutionProcessor<D>( u ),
        makeFunctionProcessor<D>( bodyForce, "BodyForce" ),
        makeVonMisesProcessor<D>( u, kinematics, material )
    };

    writeOutput( *basisPtr, postmesh, std::move( processors ), output );

    log( "done.\n" );
}

double PlaneStressAnalysis::strainEnergy( ) const
{
    log( "Integrating strain energy ... " );

    auto kinematics = mlhp::makeSmallStrainKinematics<2>( );
    auto energyIntegrand = makeInternalEnergyIntegrand( u, kinematics, material );
    auto internalEnergy = double { 0.0 };

    integrateOnDomain<2>( *basisPtr, energyIntegrand, { internalEnergy }, dirichlet );
    
    log( "done.\n" );

    return std::sqrt( internalEnergy );
}

void PlaneStressAnalysis::registerLogger( const Logger& logger )
{
    loggers.push_back( logger );
}

void PlaneStressAnalysis::log( std::string&& msg ) const
{
    for( auto& logger : loggers )
    {
        logger( msg );
    }
}

template<size_t D>
traceshared::Mesh makeCartesianMesh( std::array<size_t, D> nelements, 
                                     std::array<double, D> origin,
                                     std::array<double, D> lengths )
{
    using namespace mlhp;

    static constexpr auto ncorners = utilities::binaryPow<size_t>( D );

    auto mesh = traceshared::Mesh { };
    auto nvertices = array::add( nelements, size_t { 1 } );
    auto vgenerator = spatial::makeGridPointGenerator( nvertices, lengths, origin );

    mesh.coordinates.reserve( D * array::product( nvertices ) );
    mesh.connectivities.reserve( ncorners * array::product( nelements ) );
    mesh.offsets.reserve( array::product( nelements ) );

    // Loop over dimensions to create vertex grid
    nd::execute( nvertices, [&]( auto ijk )
    {
        auto xyz = vgenerator( ijk );
        
        for( size_t axis = 0; axis < D; ++axis )
        {
            mesh.coordinates.push_back( xyz[axis] );
        }
    } );

    auto vstrides = nd::stridesFor( nvertices );

    // Loop over dimensions to create element grid
    nd::execute( nelements, [&]( auto ijkCell )
    {
        mesh.offsets.push_back( mesh.connectivities.size( ) );

        for( size_t icorner = 0; icorner < ncorners; ++icorner )
        {
            auto localIjk = nd::binaryUnravel<size_t, D>( icorner );
            auto globalIjk = array::add( ijkCell, localIjk );
            auto vertexIndex = nd::linearIndex( vstrides, globalIjk );

            mesh.connectivities.push_back( vertexIndex );
        }
    } );

    return mesh;
}

#define INSTANTIATE_DIMENSIONS( D )                                       \
    template                                                              \
    traceshared::Mesh makeCartesianMesh( std::array<size_t, 2> nelements, \
                                         std::array<double, 2> origin,    \
                                         std::array<double, 2> lengths )

INSTANTIATE_DIMENSIONS( 2 );

}