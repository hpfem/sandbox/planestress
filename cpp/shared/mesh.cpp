#include "mesh.hpp"

#include "nlohmann/json.hpp"

#include <sstream>

namespace traceshared
{

std::string to_json(const Mesh& mesh)
{

    nlohmann::json j;
    j["trianglemesh"] = {
        {"nodes", mesh.coordinates},
        {"connectivities", mesh.connectivities},
        {"offsets", mesh.offsets}
    };

    std::stringstream ss;
    ss << j;
    return ss.str();
}

Mesh from_json(const std::string& json)
{
    nlohmann::json j = nlohmann::json::parse(json);

    auto triangle_mesh = j["trianglemesh"];
    Mesh result;
    triangle_mesh["nodes"].get_to(result.coordinates);
    triangle_mesh["connectivities"].get_to(result.connectivities);
    triangle_mesh["offsets"].get_to(result.offsets);
    return result;
}

std::string to_svg(const traceshared::Mesh& mesh)
{
    std::stringstream result;
    for(const auto& offset : mesh.offsets)
    {
        auto p0_id = mesh.connectivities[offset];
        auto p1_id = mesh.connectivities[offset + 1];
        auto p2_id = mesh.connectivities[offset + 2];
        
        std::array<double, 2> p0{mesh.coordinates[2 * p0_id], mesh.coordinates[2 * p0_id + 1]};
        std::array<double, 2> p1{mesh.coordinates[2 * p1_id], mesh.coordinates[2 * p1_id + 1]};
        std::array<double, 2> p2{mesh.coordinates[2 * p2_id], mesh.coordinates[2 * p2_id + 1]};
        result << "M" << p0[0] << " " << p0[1] << "L" << p1[0] << " " << p1[1];
        result << "M" << p1[0] << " " << p1[1] << "L" << p2[0] << " " << p2[1];
        result << "M" << p2[0] << " " << p2[1] << "L" << p0[0] << " " << p0[1];
    }

    return result.str();
}

}