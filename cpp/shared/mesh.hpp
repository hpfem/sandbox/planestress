#pragma once

#include <vector>
#include <string>

namespace traceshared
{

struct Mesh
{
    std::vector<double> coordinates;
    std::vector<size_t> connectivities;
    std::vector<size_t> offsets;
};

std::string to_json(const Mesh& mesh);
Mesh from_json(const std::string& json);
std::string to_svg(const traceshared::Mesh& mesh);

}