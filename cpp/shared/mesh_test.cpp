#include "mesh.hpp"

#include "nlohmann/json.hpp"
#include <catch2/catch_test_macros.hpp>
#include <iostream>

TEST_CASE("Mesh to json")
{
    traceshared::Mesh m{
        .coordinates{0.0, 0.0, 1.0, 0.0, 0.0, 1.0},
        .connectivities{0, 1, 2},
        .offsets{0},
    };

    auto json_string = traceshared::to_json(m);

    nlohmann::json j; 
    REQUIRE_NOTHROW(j = nlohmann::json::parse(json_string));

    REQUIRE(j.find("trianglemesh") != j.end());
    REQUIRE(j["trianglemesh"].find("nodes") != j["trianglemesh"].end());
    REQUIRE(j["trianglemesh"]["nodes"].size() == 6);

    REQUIRE(j["trianglemesh"].find("connectivities") != j["trianglemesh"].end());
    REQUIRE(j["trianglemesh"]["connectivities"].size() == 3);

    REQUIRE(j["trianglemesh"].find("offsets") != j["trianglemesh"].end());
    REQUIRE(j["trianglemesh"]["offsets"].size() == 1);
}

constexpr std::string_view mesh_json = R"""(
{"trianglemesh":{"connectivities":[0,1,2],"nodes":[0.0,0.0,1.0,0.0,0.0,1.0],"offsets":[0]}}
)""";

TEST_CASE("mesh_from_json")
{
    traceshared::Mesh mesh;
    REQUIRE_NOTHROW(mesh = traceshared::from_json(std::string{mesh_json}));
    CHECK(mesh.coordinates.size() == 6);
    CHECK(mesh.connectivities.size() == 3);
    CHECK(mesh.offsets.size() == 1);

}

TEST_CASE("mesh_to_svg")
{
    traceshared::Mesh mesh;
    mesh.coordinates = {
        0.0, 0.0, 1.0, 0.0, 0.0, 1.0
    };

    mesh.connectivities = {0, 1, 2};
    mesh.offsets = {0};
    std::string svg;
    REQUIRE_NOTHROW(svg = traceshared::to_svg(mesh));
    REQUIRE(!svg.empty());
}