import { useState, useEffect, useCallback } from 'react';
import { InteractiveEntity, InteractiveNode } from './useInteractiveModel';

export function useSnap({canvasRef, interactiveEntities}: {canvasRef: React.MutableRefObject<HTMLCanvasElement>, interactiveEntities: Array<InteractiveEntity>}){
    
    // This is a construction site
    const calculateSnap = useCallback(({x, y} : {x: number, y: number}) => {
        if(canvasRef.current == null) return {x, y};
        const boundingRect = canvasRef.current.getBoundingClientRect();
        for(const e of interactiveEntities){
            const xWorld = x;
            const yWorld = boundingRect.height - y;
            if(!(e instanceof InteractiveNode)) continue;
            if(e.getDistance({x: xWorld, y: yWorld}) < 10){
                return {x: e.node.x, y: boundingRect.height - e.node.y};
            }
        }
        return {x, y}
    },[interactiveEntities]);

    const getCloseObject = useCallback(({x, y} : {x: number, y: number}) => {
        if(canvasRef.current == null) return null;
        const xWorld = x;
        const yWorld = canvasRef.current.getBoundingClientRect().height - y;
        for(const e of interactiveEntities){
            if(e.getDistance({x: xWorld, y: yWorld}) < 10){
                return e;
            }
        }
        return null;
    },[interactiveEntities]);

    return [calculateSnap, getCloseObject] as const;
}