import { useState, useEffect, useCallback, useReducer } from 'react';
import { GeometricEntity, Node, Edge, GeometricModel } from '../model/model';
import { BoundaryCondition, BoundaryConditionKind } from '../model/simulation';

export abstract class InteractiveEntity{
    support: GeometricEntity
    isSelected: boolean;
    isHilighted: boolean;
    constructor(support: GeometricEntity){
        this.support = support;
        this.isSelected = false;
        this.isHilighted = false;
    }

    abstract draw(ctx: CanvasRenderingContext2D): void;
    abstract getDistance({x, y}: {x: number, y: number}): number;
}

export class InteractiveNode extends InteractiveEntity{
    node: Node;
    constructor(n: Node){
        super(n);
        this.node = n;
    }

    draw(context: CanvasRenderingContext2D): void {
        context.save();
        context.beginPath();
        if(this.isSelected){
            context.fillStyle = 'lime';
        }
        else if(this.isHilighted){
            context.fillStyle = 'lightblue';
        }
        else{
            context.fillStyle = 'black';
        }
        context.arc(this.node.x, this.node.y, 10.0, 0.0, 2 * Math.PI, false);
        context.fill();
        context.restore();
    }

    getDistance({ x, y }: { x: number; y: number; }): number {
        return Math.sqrt((this.node.x - x) * (this.node.x - x) + (this.node.y - y) * (this.node.y - y));
    }
};

export class InteractiveEdge extends InteractiveEntity{
    edge: Edge;

    constructor(e: Edge){
        super(e);
        this.edge = e;
    }

    draw(context: CanvasRenderingContext2D): void {
        context.save();
        context.beginPath();
        if(this.isSelected){
            context.strokeStyle = 'lime';
        } else if(this.isHilighted) {
            context.strokeStyle = 'lightblue';
        } else {
            context.strokeStyle = 'black';
        }
        context.lineWidth = 1.0;
        context.moveTo(this.edge.first.x, this.edge.first.y);
        context.lineTo(this.edge.last.x, this.edge.last.y);
        context.stroke();
        context.restore();
    }

    getDistance({ x, y }: { x: number; y: number; }): number {
        const p1 = this.edge.first;
        const p2 = this.edge.last;

        let dx = p2.x - p1.x;
        let dy = p2.y - p1.y;

        if (Math.abs(dx) <= 1E-6 && Math.abs(dy) <= 1E-6) {
            return Math.sqrt((p1.x - x) * (p1.x - x) + (p1.y - y) * (p1.y - y));
        }

        const t = ((x - p1.x) * dx + (y - p1.y) * dy) / (dx * dx + dy * dy);

        if (t < 0) {
            dx = x - p1.x;
            dy = y - p1.y;
        } else if (t > 1) {
            dx = x - p2.x;
            dy = y - p2.y;
        } else {
            const closestPoint = { x: p1.x + t * dx, y: p1.y + t * dy };
            dx = x - closestPoint.x;
            dy = y - closestPoint.y;
        }

        return Math.sqrt(dx * dx + dy * dy);
    }
};

export abstract class InteractiveBoundaryCondition extends InteractiveEntity {
    boundaryConditionSupport: BoundaryCondition;
    selectedColor: string;
    hilightedColor: string;
    defaultColor: string;

    constructor(boundaryConditionSupport: BoundaryCondition){
        super(boundaryConditionSupport.support);
        this.boundaryConditionSupport = boundaryConditionSupport;
        this.selectedColor = 'lime';
        this.hilightedColor = 'lightblue';
        this.defaultColor = 'red';
    }

    static create(boundaryCondition: BoundaryCondition) : InteractiveBoundaryCondition{
        if(boundaryCondition.kind == BoundaryConditionKind.Displacement){
            return new FixedDisplacementInteractiveBoundaryCondition(boundaryCondition);
        }
    }
}

export class FixedDisplacementInteractiveBoundaryCondition extends InteractiveBoundaryCondition{
    constructor(support: BoundaryCondition){
        super(support);
    }

    draw(context: CanvasRenderingContext2D): void{
        // Make the node red if it is fixed
        if (this.support instanceof Node) {
            const node = this.support as Node;
            context.save();
            context.beginPath();
            context.lineWidth = 3;
            if (this.isSelected) {
                context.strokeStyle = this.selectedColor;
            }
            else if (this.isHilighted) {
                context.strokeStyle = this.hilightedColor;
            }
            else {
                context.strokeStyle = this.defaultColor;
            }
            context.arc(node.x, node.y, 10.0, 0.0, 2 * Math.PI, false);
            context.stroke();
            context.restore();
        }
        else if (this.support instanceof Edge) {
            const edge = this.support as Edge;
            context.save();
            context.beginPath();
            if (this.isSelected) {
                context.strokeStyle = this.selectedColor;
            } else if (this.isHilighted) {
                context.strokeStyle = this.hilightedColor;
            } else {
                context.strokeStyle = this.defaultColor;
            }
            context.lineWidth = 4.0;
            context.setLineDash([10, 10]);
            context.moveTo(edge.first.x, edge.first.y);
            context.lineTo(edge.last.x, edge.last.y);
            context.stroke();
            context.restore();
        }
    }

    getDistance({ x, y }: { x: number; y: number; }): number {
        if (this.support instanceof Node) {
            const node = this.support as Node;
            return Math.sqrt((node.x - x) * (node.x - x) + (node.y - y) * (node.y - y));
        }
        return Number.MAX_VALUE;
    }
}

export type InteractiveModel = {
    nodes: Array<InteractiveNode>,
    edges: Array<InteractiveEdge>,
    boundaryConditions: Array<InteractiveBoundaryCondition>
};

export type InteractiveModelAction = 
{type: "refreshAll", payload: {model: GeometricModel, boundaryConditions: Array<BoundaryCondition>}} |
{type: "setHilighted", payload: string} | 
{type: "setSelected", payload: string};

const dispatchInteractiveModel = (previous: InteractiveModel, action: InteractiveModelAction): InteractiveModel => {
    switch(action.type){
        case "setHilighted":
            return {
                nodes: previous.nodes.map((n) => {n.isHilighted = n.support.id == action.payload; return n;}),
                edges: previous.edges.map((e) => {e.isHilighted = e.support.id == action.payload; return e;}),
                boundaryConditions: previous.boundaryConditions.map((b) => {b.isHilighted = b.support.id == action.payload; return b;})
            };
        case "setSelected":
            return {
                nodes: previous.nodes.map((n) => {n.isSelected = n.support.id == action.payload; return n;}),
                edges: previous.edges.map((e) => {e.isSelected = e.support.id == action.payload; return e;}),
                boundaryConditions: previous.boundaryConditions.map((b) => {b.isSelected = b.support.id == action.payload; return b;})
            };
        case "refreshAll":
            return {
                nodes: action.payload.model.nodes.map((n) => {return new InteractiveNode(n);}),
                edges: action.payload.model.edges.map((e) => {return new InteractiveEdge(e);}),
                boundaryConditions: action.payload.boundaryConditions.map((b) => InteractiveBoundaryCondition.create(b))
            }
    }
}

export function useInteractiveModel({model, boundaryConditions} : {model: GeometricModel, boundaryConditions: Array<BoundaryCondition>}){
    const [interactiveModel, interactiveModelReducer] = useReducer(dispatchInteractiveModel, {nodes: new Array<InteractiveNode>(), edges: new Array<InteractiveEdge>(), boundaryConditions: []});

    useEffect(() => {
        interactiveModelReducer({type: "refreshAll", payload: {model, boundaryConditions}});
    }, [model, boundaryConditions]);

    return [interactiveModel, interactiveModelReducer] as const;
}