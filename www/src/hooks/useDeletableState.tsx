import { useState, useEffect } from 'react';
import * as Emtrace from 'emtrace-types'

interface DeletableType{
    delete: () => void
}

export function useDeletableState<T extends DeletableType>(initialValue: T){
    const [deletableState, setDeletableState] = useState<T>(initialValue);

    useEffect(() => {
        return () => {
            if(deletableState != null)
            {
                deletableState.delete();
            }
        };
    },[deletableState]);

    return [deletableState, setDeletableState] as const;
}

