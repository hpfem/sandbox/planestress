import * as React from 'react';
import { InteractiveNode, InteractiveModelAction, InteractiveModel } from "../hooks/useInteractiveModel";
import { GeometryAction, GeometricEntity } from '../model/model';
import { BoundaryConditionComponent } from './BoundaryConditionComponent';
import { BoundaryCondition, BoundaryConditionKind, BoundaryConditionAction } from '../model/simulation';

export interface NodeProps {
    interactiveNode: InteractiveNode,
    interactiveModel: InteractiveModel,
    interactiveModelDispatch: (arg0: InteractiveModelAction) => void,
    geometricModelDispatch: (arg0: GeometryAction) => void,
    dispatchBoundaryConditions: (arg0: BoundaryConditionAction) => void,
};

export const NodeComponent = ({interactiveNode,
                               interactiveModel,
                               interactiveModelDispatch,
                               geometricModelDispatch,
                               dispatchBoundaryConditions}: NodeProps) => {

    const handleNodeChange = (id: string, x: string, y: string) => {
        const newX = parseFloat(x);
        const newY = parseFloat(y);
        geometricModelDispatch({type: "changeNode", payload: {id, x: newX, y: newY}});
    }

    const handleNewBoundaryCondition = (obj: GeometricEntity) => {
        const newBC = new BoundaryCondition(obj, BoundaryConditionKind.Displacement, null, null);
        dispatchBoundaryConditions({type: 'add', payload: newBC});
    };

    const handleChangeBoundaryCondition = (bc: BoundaryCondition) => {
        dispatchBoundaryConditions({type: "update", payload: bc});
    };

    const handleRemoveBoundaryCondition = (bc: BoundaryCondition) => {
        dispatchBoundaryConditions({type: "remove", payload: bc.id});
    };


    return (
        <div
            onFocus={() => { interactiveModelDispatch({ type: "setSelected", payload: interactiveNode.node.id }); }}
            onMouseOver={() => { interactiveModelDispatch({ type: "setHilighted", payload: interactiveNode.node.id }); }}
        >
            <input
                style={{ width: '80px', fontWeight: interactiveNode.isSelected && 'bold' }}
                type="number"
                onChange={(e) => handleNodeChange(interactiveNode.node.id, e.target.value, interactiveNode.node.y.toString())}
                value={interactiveNode.node.x}
            />
            <input
                style={{ width: '80px', fontWeight: interactiveNode.isSelected && 'bold' }}
                type="number"
                onChange={(e) => handleNodeChange(interactiveNode.node.id, interactiveNode.node.x.toString(), e.target.value)}
                value={interactiveNode.node.y}
            />
            <BoundaryConditionComponent
                bc={interactiveModel.boundaryConditions.find((bc) => bc.support == interactiveNode.support)}
                handleNewBoundaryCondition={() => handleNewBoundaryCondition(interactiveNode.node)}
                handleChangeBoundaryCondition={handleChangeBoundaryCondition}
                handleRemoveBoundaryCondition={handleRemoveBoundaryCondition}
            />
        </div>
    );

}