import * as React from 'react';
import { BoundaryCondition, BoundaryConditionKind } from '../model/simulation';
import { InteractiveBoundaryCondition } from '../hooks/useInteractiveModel';

export interface BoundaryConditionComponentProps{
    bc?: InteractiveBoundaryCondition,
    handleNewBoundaryCondition: () => void
    handleChangeBoundaryCondition: (arg0: BoundaryCondition) => void
    handleRemoveBoundaryCondition: (arg0: BoundaryCondition) => void
};

export const BoundaryConditionComponent = ({bc, handleNewBoundaryCondition, handleChangeBoundaryCondition, handleRemoveBoundaryCondition} : BoundaryConditionComponentProps) => {

    const handleChangeBCKind = (e: React.ChangeEvent<HTMLSelectElement>) => {
        if(e.target.value == "displacement"){
            bc.boundaryConditionSupport.kind = BoundaryConditionKind.Displacement;
        } else {
            bc.boundaryConditionSupport.kind = BoundaryConditionKind.Force;
        }
        handleChangeBoundaryCondition(bc.boundaryConditionSupport);
    };

    const handleToggleBCComponent = (which: number) => {
        if (which == 0) {
            bc.boundaryConditionSupport.xComponent = (bc.boundaryConditionSupport.xComponent == null) ? 0.0 : null;
        } else {
            bc.boundaryConditionSupport.yComponent = (bc.boundaryConditionSupport.yComponent == null) ? 0.0 : null;
        }
        handleChangeBoundaryCondition(bc.boundaryConditionSupport);
    };

    const handleUpdateBCComponent = (e: React.ChangeEvent<HTMLInputElement>, which: number) => {
        if(which == 0) {
            bc.boundaryConditionSupport.xComponent = parseFloat(e.target.value);
            handleChangeBoundaryCondition(bc.boundaryConditionSupport);
            return;
        }
        bc.boundaryConditionSupport.yComponent = parseFloat(e.target.value);
        handleChangeBoundaryCondition(bc.boundaryConditionSupport);
    };

    if(bc == null){
        return (<button onClick={(e) => handleNewBoundaryCondition()}>Add B.C.</button>);
    }

    return (
        <div style={{display: 'inline'}}>
            <button onClick={() => handleRemoveBoundaryCondition(bc.boundaryConditionSupport)}>Remove B.C.</button>
            <select onChange={handleChangeBCKind}>
                <option value="force" selected={bc.boundaryConditionSupport.kind == BoundaryConditionKind.Force}>Force</option>
                <option value="displacement" selected={bc.boundaryConditionSupport.kind == BoundaryConditionKind.Displacement}>Displacement</option>
            </select>
            <input type="checkbox" onChange={() => handleToggleBCComponent(0)} checked={bc.boundaryConditionSupport.xComponent == undefined ? false : true }/>
            <input type="number" onChange={(e) => handleUpdateBCComponent(e, 0)} disabled={bc.boundaryConditionSupport.xComponent == undefined ? true : false} value={bc.boundaryConditionSupport.xComponent == undefined ? 0.0 : bc.boundaryConditionSupport.xComponent }/>
            <input type="checkbox" onChange={() => handleToggleBCComponent(1)} checked={bc.boundaryConditionSupport.yComponent == undefined ? false : true }/>
            <input type="number" onChange={(e) => handleUpdateBCComponent(e, 1)} disabled={bc.boundaryConditionSupport.yComponent == undefined ? true : false} value={bc.boundaryConditionSupport.yComponent == undefined ? 0.0 : bc.boundaryConditionSupport.yComponent }/>
        </div>
    );
};
