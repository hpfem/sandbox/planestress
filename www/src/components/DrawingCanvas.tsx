import * as React from 'react';
import { useRef, useState, useEffect} from 'react';

import { Node, Edge, GeometricEntity, GeometricModel } from '../model/model'
import { InteractiveModel, InteractiveNode, InteractiveEdge, InteractiveEntity} from '../hooks/useInteractiveModel';
import { v4 as uuid } from 'uuid';
import { useSnap } from '../hooks/useSnap';
import { ActiveInteractionMode } from '..';

// A point has coordinates in canvas coordinates
export type Point2D = {
    x: number,
    y: number
};

const clearCanvas = (canvas: HTMLCanvasElement) => {
    const context = canvas.getContext('2d');
    context.fillStyle = 'lightgray';
    context.fillRect(0, 0, canvas.width, canvas.height);
}


// Utility functions, I might move them to a different file later
const renderObjects = (canvas: HTMLCanvasElement, interactiveModel: InteractiveModel, model: GeometricModel, pointer: Point2D | null, lastCreatedNode: Node | null, drawRubberBand: boolean, hilight? : GeometricEntity, selected? : GeometricEntity) => 
{
    const context = canvas.getContext('2d');

    context.save();
    context.scale(1, -1);
    context.translate(0, -canvas.height);
    [...interactiveModel.edges,
     ...interactiveModel.nodes,
     ...interactiveModel.boundaryConditions].forEach((e) => e.draw(context));
    context.restore();

    if(pointer)
    {
        context.fillStyle = 'blue';
        context.beginPath();
        context.arc(pointer.x, pointer.y, 5.0, 0.0, 2 * Math.PI, false);
        context.fill();

        if(lastCreatedNode != null && drawRubberBand)
        {
            const lastPoint = toPoint(canvas, lastCreatedNode);
            context.beginPath();
            context.fillStyle = "blue";
            context.moveTo(lastPoint.x, lastPoint.y);
            context.lineTo(pointer.x, pointer.y);
            context.stroke();
        }
    }
};

const renderMesh = (canvas: HTMLCanvasElement, meshSVG: string) => {
        const context = canvas.getContext('2d');
        context.save();
        context.scale(1, -1);
        context.translate(0, -canvas.height);
        const path = new Path2D(meshSVG);
        context.stroke(path);
        context.restore();
}

const renderTrajectories = (canvas: HTMLCanvasElement, svgTrajectories: string) => {
    const context = canvas.getContext('2d');
    context.save();
    if (svgTrajectories != "") {
        context.scale(1, -1);
        context.translate(0, -canvas.height);
        const coloredTrajectories = new Map(Object.entries(JSON.parse(svgTrajectories)));
        coloredTrajectories.forEach((path: string, color: string) => {
            const trajectoriesPath = new Path2D(path);
            context.strokeStyle = color;
            context.lineWidth = 3;
            context.stroke(trajectoriesPath);
        });
        context.restore();
    }
}

const getDistance = (a: Point2D | Node, b: Point2D | Node) => {
    return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

const toPoint = (canvas: HTMLCanvasElement, n: Node | Point2D) => {
    const boundingRect = canvas.getBoundingClientRect();
    return {x: n.x, y: boundingRect.height - n.y};
}

const isSamePoint = (a: Point2D, b: Point2D) => {
    return getDistance(a, b) < 1E-6;
}


type DrawingCanvasProps = {
    model: GeometricModel,
    meshSVG: string,
    trajectories: string,
    interactiveModel: InteractiveModel,
    activeInteractionMode: ActiveInteractionMode,
    onNewNode: (arg0: Node) => void,
    onNewEdge: (arg0: Edge) => void
    onNewLoop: () => void
    onNewSeedPoint: (arg0: {x: number, y: number}) => void
    setSelected: (arg0: string) => void,
    setHilighted: (arg0: string) => void,
    isMeshVisible: boolean
};

export const DrawingCanvas = ({model, meshSVG, trajectories, interactiveModel, activeInteractionMode, onNewNode, onNewEdge, onNewLoop, onNewSeedPoint, setSelected, setHilighted, isMeshVisible} : DrawingCanvasProps) => {
    const [mousePosition, setMousePosition] = useState<Point2D | null>(null);
    const [isDrawing, setIsDrawing] = useState(true);
    const canvas = useRef<HTMLCanvasElement>(null);
    const [calculateSnap, getCloseObject] = useSnap({canvasRef: canvas, interactiveEntities: [...interactiveModel.nodes, ...interactiveModel.edges]});
    const [lastCreatedNode, setLastCreatedNode] = useState<Node>(null);

    useEffect( () => {
        if(!canvas.current) return;
        clearCanvas(canvas.current);
        if (activeInteractionMode == ActiveInteractionMode.Drawing) {
            if(isMeshVisible){
                renderMesh(canvas.current, meshSVG);
            }
            renderObjects(canvas.current, interactiveModel, model, mousePosition, lastCreatedNode, isDrawing);
        } else {
            if(isMeshVisible){
                renderMesh(canvas.current, meshSVG);
            }
            renderObjects(canvas.current, interactiveModel, model, mousePosition, lastCreatedNode, isDrawing);
            renderTrajectories(canvas.current, trajectories);
        }
    },[model, meshSVG, trajectories, interactiveModel, lastCreatedNode, mousePosition, isDrawing, activeInteractionMode, isMeshVisible]);

    useEffect( () => {
        if(model.nodes.length == 0) {
            setMousePosition(null);
            setIsDrawing(true);
            setSelected("");
            setHilighted("");
        }
    },[model]);

    const handleMouseMove = (e: React.MouseEvent<HTMLCanvasElement> ) => {
        const boundingRect = canvas.current.getBoundingClientRect();
        const x = e.clientX - boundingRect.left;
        const y = e.clientY - boundingRect.top;
        const snappedPos = calculateSnap({x, y});
        if(!isDrawing){
            const objectUnderMouse = getCloseObject({x: x, y: y});
            if(objectUnderMouse){
                setHilighted(objectUnderMouse.support.id);
            } else {
                setHilighted("");
            }
        }
        setMousePosition(snappedPos);
    };

    const handleMouseDown = (e: React.MouseEvent<HTMLCanvasElement>) => {
        if(!canvas.current) return;
        const boundingRect = canvas.current.getBoundingClientRect();

        let x = e.clientX - boundingRect.left;
        let y = e.clientY - boundingRect.top;
        if(activeInteractionMode == ActiveInteractionMode.Simulation){
            onNewSeedPoint({x: x, y: boundingRect.height - y});
            return;
        }

        const snappedPos = calculateSnap({x: x, y: y});

        x = snappedPos.x;
        y = snappedPos.y;


        if (e.button == 0) {
            const objectUnderMouse = getCloseObject({x: x, y: y});
            if (!isDrawing) {
                if (objectUnderMouse) {
                    setSelected(objectUnderMouse.support.id);
                    return;
                }
                setSelected("");
            }
            else {
                const lastLoop = model.loops.at(model.loops.length - 1);
                // We have more than two edges, so the last loop can be closed
                if (lastLoop?.edges.length > 2) {
                    const firstNode = lastLoop.edges[0].first;
                    const lastNode = lastLoop.edges[lastLoop.edges.length - 1].last;
                    if (isSamePoint(toPoint(canvas.current, firstNode), { x: x, y: y })) {
                        onNewEdge(new Edge(lastNode, firstNode));
                        setLastCreatedNode(null);
                        onNewLoop();
                        return;
                    }
                }

                // There's already a node, so it is possible to draw a rubber edge
                if (lastCreatedNode != null) {
                    const lastNode = lastCreatedNode;
                    const newNode = new Node(x, boundingRect.height - y); 
                    onNewEdge(new Edge(lastNode, newNode));
                    onNewNode(newNode);
                    setLastCreatedNode(newNode);
                    return;
                }
                if( model.nodes.length == 0){
                    // We are beginning a new loop here
                    onNewLoop();
                }

                const newNode = new Node(x, boundingRect.height - y);

                onNewNode(newNode);
                setLastCreatedNode(newNode);
            }
        }
        if (e.button == 2) {
            setIsDrawing(false);
            if (model.nodes.length > 1) {
                const firstNode = model.nodes[0];
                const lastNode = model.nodes[model.nodes.length - 1];
                onNewEdge(new Edge(lastNode, firstNode));
            }
        }
        e.preventDefault();
        e.stopPropagation();
    };

    return (
        <div>
            <div>
                <canvas ref={canvas} onMouseMove={handleMouseMove} onMouseDown={handleMouseDown} width={800} height={600} onContextMenu={(e) => { e.preventDefault(); }}></canvas>
            </div>
            <span>Drawing</span><input type="checkbox" onChange={(e) => {setIsDrawing(!isDrawing)}} checked={isDrawing}/>
        </div>
    );

};
