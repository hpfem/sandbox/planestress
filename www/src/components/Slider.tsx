import * as React from 'react';
import { useRef, useState, useEffect} from 'react';

export const SliderWithEditBox = ({
    minValue,
    maxValue,
    step,
    value,
    onValueChanged,
} : {
    minValue: number,
    maxValue: number,
    step: number
    value: number,
    onValueChanged: (arg0: number) => void
}) => {
    const handleValueChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        onValueChanged(parseFloat(e.target.value));
    };
    return (
        <div>
        <input type="range" min={minValue} max={maxValue} step={step} value={value} onChange={handleValueChanged}></input>
        <input type="number" min={minValue} max={maxValue} step={step} value={value} onChange={handleValueChanged}></input>
        </div>
    );
};