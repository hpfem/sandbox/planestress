import * as React from 'react';
import { InteractiveEdge, InteractiveModelAction, InteractiveModel } from "../hooks/useInteractiveModel";
import { GeometryAction, GeometricEntity } from '../model/model';
import { BoundaryConditionComponent } from './BoundaryConditionComponent';
import { BoundaryCondition, BoundaryConditionKind, BoundaryConditionAction } from '../model/simulation';

export interface EdgeProps {
    interactiveEdge: InteractiveEdge,
    interactiveModel: InteractiveModel,
    interactiveModelDispatch: (arg0: InteractiveModelAction) => void,
    geometricModelDispatch: (arg0: GeometryAction) => void,
    dispatchBoundaryConditions: (arg0: BoundaryConditionAction) => void,
};

export const EdgeComponent = ({interactiveEdge,
                               interactiveModel,
                               interactiveModelDispatch,
                               geometricModelDispatch,
    dispatchBoundaryConditions }: EdgeProps) => {

    const handleNewBoundaryCondition = (obj: GeometricEntity) => {
        const newBC = new BoundaryCondition(obj, BoundaryConditionKind.Displacement, null, null);
        dispatchBoundaryConditions({ type: 'add', payload: newBC });
    };

    const handleChangeBoundaryCondition = (bc: BoundaryCondition) => {
        dispatchBoundaryConditions({ type: "update", payload: bc });
    };

    const handleRemoveBoundaryCondition = (bc: BoundaryCondition) => {
        dispatchBoundaryConditions({ type: "remove", payload: bc.id });
    };


    return (
        <div
            style={{ fontWeight: interactiveEdge.isSelected && 'bold', color: interactiveEdge.isHilighted && "lightblue" }}
            onClick={() => { interactiveModelDispatch({ type: "setSelected", payload: interactiveEdge.support.id }); }}
            onMouseOver={() => { interactiveModelDispatch({ type: "setHilighted", payload: interactiveEdge.support.id }); }}
        >
            <span style={{ width: 250, display: 'inline-block' }}>
                ({interactiveEdge.edge.first.x}, {interactiveEdge.edge.first.y}) to ({interactiveEdge.edge.last.x}, {interactiveEdge.edge.last.y})
            </span>
            <BoundaryConditionComponent
                bc={interactiveModel.boundaryConditions.find((bc) => bc.support == interactiveEdge.support)}
                handleNewBoundaryCondition={() => handleNewBoundaryCondition(interactiveEdge.support)}
                handleChangeBoundaryCondition={handleChangeBoundaryCondition}
                handleRemoveBoundaryCondition={handleRemoveBoundaryCondition}
            />
        </div>
    );

}