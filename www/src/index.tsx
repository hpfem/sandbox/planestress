import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import { StrictMode, useState, useEffect, useRef, useReducer } from 'react';

import Module from 'emtrace';
import * as Gmsh from 'emgmsh';
import * as Emtrace from 'emtrace-types';
import * as Emgmsh from 'emgmsh-types';

import { DrawingCanvas } from './components/DrawingCanvas';
import { NodeComponent } from './components/Node';
import { EdgeComponent } from './components/EdgeComponent';
import { SliderWithEditBox } from './components/Slider';
import { useDeletableState } from './hooks/useDeletableState';
import { Node, Edge, Loop, GeometricModel, GeometricEntity, geometryReducer} from './model/model';
import { InteractiveEdge, InteractiveNode, useInteractiveModel } from './hooks/useInteractiveModel';
import { BoundaryCondition, BoundaryConditionKind, boundaryConditionReducer } from './model/simulation';


type Mesh = {
    trianglemesh: {
    connectivities: Array<number>,
    nodes: Array<number>,
    offsets: Array<number>
    }
};

export enum ActiveInteractionMode{
    Drawing,
    Simulation
}


const App: React.FC = () => {

    const [wasm, setWasm] = useState<Emtrace.MainModule>(null);
    const [gmsh, setGmsh] = useState<Emgmsh.MainModule>(null);
    const [svgTrajectories, setSvgTrajectories] = useState<string>("");
    const [planeStresssAnalysis, setPlaneStressAnalysis] = useDeletableState<Emtrace.PlaneStressAnalysis>(null);
    const [geometricModel, dispatchGeometricModel] = useReducer(geometryReducer, new GeometricModel([], [], []));
    const [boundaryConditions, dispatchBoudaryConditions] = useReducer(boundaryConditionReducer, new Array<BoundaryCondition>());
    const [interactiveModel, interactiveModelReducer] = useInteractiveModel({model: geometricModel, boundaryConditions: boundaryConditions});
    const [mesh, setMesh] = useState<string>("");
    const [meshSizeFactor, setMeshSizeFactor] = useState<number>(1.0);
    const [meshData, setMeshData] = useState<Mesh>(null);
    const [meshJSON, setMeshJSON] = useState<string>("");
    const [uuidToNodes, setUuidToNodes] = useState<Map<string, Array<number>>>(null);
    const [activeInterationMode, setActiveInteractionMode] = useState(ActiveInteractionMode.Drawing);
    const [isMeshVisible, setIsMeshVisible] = useState<boolean>(true);

    useEffect(() => {
        Module().then((wasmModule: Emtrace.MainModule) => setWasm(wasmModule));
        Gmsh.default().then((gmsh: Emgmsh.MainModule) => setGmsh(gmsh));
    }, []);

    const addNewSeedPoint = (p: Node) => {
        if(!planeStresssAnalysis) return;
        const emSeed = new wasm.Point2D();
        emSeed.x = p.x;
        emSeed.y = p.y;
        planeStresssAnalysis.addTrace(emSeed);
        setSvgTrajectories(planeStresssAnalysis.svgTrajectories());
        emSeed.delete();
    };

    const onNewNode = (p: Node) => {
        dispatchGeometricModel({type: "newNode", payload: p});
    };
    
    const onNewEdge = (e: Edge) => {
        dispatchGeometricModel({type: "newEdge", payload: e});
    };

    const onNewLoop = () => {
        dispatchGeometricModel({type: 'startNewLoop', payload: null });
    }

    const handleGenerateMesh = () => {
        console.log(geometricModel.toJSON());
        const meshResult = gmsh.mesh_json(geometricModel.toJSON(), meshSizeFactor);
        const mesh = meshResult.mesh;
        const uuid_to_nodes = meshResult.uuid_to_nodes;
        const svg = gmsh.mesh_to_svg(mesh);
        const mesh_json = gmsh.mesh_to_json(mesh);
        console.log("Computed the following mesh\n", mesh_json);
        setMesh(svg);
        setMeshData(JSON.parse(mesh_json));
        setMeshJSON(mesh_json);
        setUuidToNodes(new Map(Object.entries(JSON.parse(gmsh.uuid_map_to_json(uuid_to_nodes)))));
        console.log(uuidToNodes);
        mesh.delete();
        meshResult.delete();
    };

    const handleRunPlaneStressAnalysis = () => {
        console.log(mesh);
        const analysis = new wasm.PlaneStressAnalysis(meshJSON);
        analysis.turnOnLogger();
        analysis.setMaterialParameters(1, 0.3);
        boundaryConditions.forEach((bc: BoundaryCondition) => {
            if(bc.kind == BoundaryConditionKind.Force) return;
            const nodes = uuidToNodes.get(bc.support.id);
            nodes.forEach((nodeId: number) => {
                if(bc.xComponent != null) analysis.fixNode(nodeId, 0, bc.xComponent);
                if(bc.yComponent != null) analysis.fixNode(nodeId, 1, bc.yComponent);
            });
        });
        analysis.solve();
        setPlaneStressAnalysis(analysis);
        setActiveInteractionMode(ActiveInteractionMode.Simulation);
    };

    const clearTrajectories = () => {
        if(planeStresssAnalysis != null)
        {
            planeStresssAnalysis.clearTraces();
            setSvgTrajectories(planeStresssAnalysis.svgTrajectories());
        }
    };

    if (wasm == null || gmsh == null) {
        return <div>Loading wasm module</div>
    }
    else {
        return (
            <div>
                <DrawingCanvas
                    model={geometricModel}
                    meshSVG={mesh}
                    trajectories={svgTrajectories}
                    interactiveModel={interactiveModel}
                    activeInteractionMode={activeInterationMode}
                    onNewNode={onNewNode}
                    onNewEdge={onNewEdge}
                    onNewLoop={onNewLoop}
                    onNewSeedPoint={addNewSeedPoint}
                    setSelected={(id: string) => interactiveModelReducer({type: "setSelected", payload: id})}
                    setHilighted={(id: string) => interactiveModelReducer({type: "setHilighted", payload: id})}
                    isMeshVisible={isMeshVisible}
                />
                <button onClick={(e) => {
                    dispatchGeometricModel({ type: "clearModel", payload: null });
                    dispatchBoudaryConditions({type: "clear"});
                    setMesh("");
                    setMeshJSON("");
                    setPlaneStressAnalysis(null);
                    setActiveInteractionMode(ActiveInteractionMode.Drawing);
                }}>Reset</button>
                <button onClick={(e) => clearTrajectories()}>Clear trajectories</button>
                <div>
                <span>Mesh size factor:</span><SliderWithEditBox minValue={0.05} maxValue={2.0} step={0.05} value={meshSizeFactor} onValueChanged={(value: number) => setMeshSizeFactor(value)}/>
                <button onClick={(e) => {handleGenerateMesh();}}>Generate mesh</button>
                </div>
                <div>
                    <span>Show mesh</span>
                    <input type='checkbox' onChange={(e) => setIsMeshVisible((prev: boolean) => !prev)} checked={isMeshVisible} disabled={mesh==''}/>
                </div>
                <button onClick={(e) => {handleRunPlaneStressAnalysis();}} disabled={meshData == null}>Run analysis</button>
                <p>Recorded nodes:</p>
                <div>
                    {
                        interactiveModel.nodes.map((n: InteractiveNode) => {
                            return (
                            <NodeComponent
                                interactiveNode={n} 
                                interactiveModel={interactiveModel} 
                                interactiveModelDispatch={interactiveModelReducer}
                                geometricModelDispatch={dispatchGeometricModel}
                                dispatchBoundaryConditions={dispatchBoudaryConditions}/>
                            )
                        })
                    }
                </div>
                <p>Recorded edges:</p>
                <div>
                    {
                        interactiveModel.edges.map((e: InteractiveEdge) => {
                            return (
                            <EdgeComponent
                            dispatchBoundaryConditions={dispatchBoudaryConditions}
                            geometricModelDispatch={dispatchGeometricModel}
                            interactiveEdge={e}
                            interactiveModel={interactiveModel}
                            interactiveModelDispatch={interactiveModelReducer}
                            />
                            )
                        })
                    }
                </div>
            </div>
        );
    }
};

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(<StrictMode><App /></StrictMode>);