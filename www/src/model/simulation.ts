import { GeometricEntity } from "./model";
import { v4 as uuid} from 'uuid';

export enum BoundaryConditionKind{
    Displacement,
    Force
}

export class BoundaryCondition{
    private id_: string;
    support: GeometricEntity;
    kind: BoundaryConditionKind;
    xComponent: number | null;
    yComponent: number | null;

    constructor(support: GeometricEntity, kind: BoundaryConditionKind, xComponent: number | null, yComponent: number | null){
        this.id_ = uuid();
        this.support = support;
        this.kind = kind;
        this.xComponent = null;
        this.yComponent = null;
    }

    get id(): string{
        return this.id_;
    }
}

export type BoundaryConditionAction = 
{type: "update", payload: BoundaryCondition} |
{type: "remove", payload: string } |
{type: "add", payload: BoundaryCondition} |
{type: "clear"};

export const boundaryConditionReducer = (previousState: Array<BoundaryCondition>, action: BoundaryConditionAction) : Array<BoundaryCondition> => {
    switch(action.type){
        case "update":
            return previousState.map((bc) => {
                if(action.payload.id == bc.id){
                    return action.payload;
                }
                return bc;
            });
        case "remove":
            const res = previousState.filter((bc) => { return bc.id != action.payload;});
            return res;
        case "add":
            return [...previousState, action.payload];
        case "clear":
            return [];
    }
};