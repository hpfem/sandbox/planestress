import {v4 as uuid} from 'uuid';

abstract class GeometricEntity{
    private _id: string;
    constructor(){
        this._id = uuid();
    }

    get id(){
        return this._id;
    }

    // Remove this later
    abstract isPoint(): boolean;
}

class Node extends GeometricEntity{
    x: number;
    y: number;

    constructor(x: number, y: number){
        super();
        this.x = x;
        this.y = y;
    }

    isPoint(): boolean {
        return true;
    }
}

class Edge extends GeometricEntity{
    first: Node;
    last: Node;

    constructor(first: Node, last: Node){
        super();
        this.first = first;
        this.last = last;
    }

    isPoint(): boolean {
        return false;
    }
}

class Loop extends GeometricEntity{
    edges: Array<Edge>;

    constructor(edges: Array<Edge>){
        super();
        this.edges = edges;
    }

    isPoint(): boolean{
        return false;
    }
}

class GeometricModel{
    nodes: Array<Node>;
    edges: Array<Edge>;
    loops: Array<Loop>;

    constructor(nodes: Array<Node>, edges: Array<Edge>, loops: Array<Loop>){
        this.nodes = nodes;
        this.edges = edges;
        this.loops = loops;
    }

    toJSON() : string {
        return JSON.stringify(
            {
                nodes: this.nodes.map((n: Node) => {return {id: n.id, x: n.x, y: n.y }}),
                edges: this.edges.map((e: Edge) => {return {id: e.id, first_node: e.first.id, second_node: e.last.id}}),
                loops: this.loops.map((l: Loop) => {return {id: l.id, edges: l.edges.map((e: Edge) => e.id)}})
            }
        );
    }
};

export type GeometryAction = 
    {type: "newNode", payload: Node} |
    {type: "changeNode", payload:{id: string, x: number, y: number}} |
    {type: "newEdge", payload: Edge} |
    {type: "startNewLoop", payload: null} |
    {type: "clearModel", payload: null};



export function geometryReducer(state: GeometricModel, action: GeometryAction){
        switch (action.type) {
        case "newNode":
            return new GeometricModel([...state.nodes, action.payload], state.edges, state.loops);
        case "changeNode":
            const newState = new GeometricModel(state.nodes, state.edges, state.loops);
            const nodeToChange = newState.nodes.find(n => n.id == action.payload.id);
            nodeToChange.x = action.payload.x;
            nodeToChange.y = action.payload.y;
            return newState;
        case "newEdge":
            const lastLoop = state.loops[state.loops.length - 1];
            lastLoop.edges.push(action.payload);
            return new GeometricModel(state.nodes, [...state.edges, action.payload], state.loops);
        case "startNewLoop":
            return new GeometricModel(state.nodes, state.edges, [...state.loops, new Loop([])]);
        case "clearModel":
            return new GeometricModel([], [], []);
    }
};


export { GeometricEntity, Node, Edge, Loop, GeometricModel };