import numpy as np
import matplotlib.pyplot as plt

# This is just a development script to print trajectories quickly

points = []
with open("build/trajectories.txt", 'r') as inFile:
    lines = inFile.readlines()
    for line in lines:
        point = [float(x) for x in line.split(' ')]
        points.append(point)


points = np.array(points)
print(points.shape)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(points[:,0], points[:,1], 'o')
ax.set_xlim(0.0, 2.0)
ax.set_ylim(0.0, 0.5)

ax.set_aspect('equal')
plt.show()